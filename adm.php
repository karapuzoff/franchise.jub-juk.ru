<?php
class adm {
   /**
    *
    * Конструктор и футер класса
    *
    *
    */
   public function __construct() {
      $this->db = new mysqlcrud();
      $this->db->connect();
      $this->content = new template();

      $this->auth();

      if (master::isLogin()) {
         if (isset($_GET['logout']))
            $this->logingOut();

         /*echo $this->content->design('adm','header');
         echo $this->content->design('adm','header-nav');
         echo $this->content->design('adm','top-nav');
         echo $this->content->design('adm','left-nav');/**/
      } else {
         if (isset($_POST['login']))
            $this->logingIn();

         echo $this->content->design('adm','header');
         echo $this->content->design('adm','auth');

         die;
      }
   }
   private function __footer() {
      echo $this->content->design('adm','footer');
   }

   private function auth() {
      if (isset($_SESSION['login'])) {
         if(isset($_COOKIE['login']) && isset($_COOKIE['hash'])) {

            // убиваем куки
            setcookie("login", "", time() - 1, '/','.'.cfg::HTTP_HOST());
            setcookie("hash","", time() - 1, '/','.'.cfg::HTTP_HOST());
            // ставим новые
            setcookie("login", $_COOKIE['login'], time() + 50000, '/','.'.cfg::HTTP_HOST());
            setcookie("hash", $_COOKIE['hash'], time() + 50000, '/','.'.cfg::HTTP_HOST());

            return true;
         } else {
            if ($_SESSION['login'] == cfg::AUTH_LOGIN) {
               setcookie("login", cfg::AUTH_LOGIN, time()+50000, '/','.'.cfg::HTTP_HOST());
               setcookie("hash", md5(cfg::AUTH_LOGIN.cfg::AUTH_PASS), time()+50000, '/','.'.cfg::HTTP_HOST());

               return true;
            } else
               return false;
         }
      } else { //если сессии нет, то проверим существование cookie. Если они существуют, то проверим их валидность по БД
         if (isset($_COOKIE['login']) && isset($_COOKIE['hash'])) {
            if ( ($_COOKIE['login'] == cfg::AUTH_LOGIN) && (md5(cfg::AUTH_LOGIN.cfg::AUTH_PASS) == $_COOKIE['hash']) ) {
               $_SESSION['login'] = cfg::AUTH_LOGIN;

               return true;
            } else {
               setcookie("login", "", time() - 360000, '/','.'.cfg::HTTP_HOST());
               setcookie("hash", "", time() - 360000, '/','.'.cfg::HTTP_HOST());
               return false;
            }
         } else { //если куки не существуют
            return false;
         }
      }
   }
   private function logingIn() {
      if ( ($_POST['login'] == cfg::AUTH_LOGIN) && ($_POST['password'] == cfg::AUTH_PASS) ) {
         setcookie ("login", $_POST['login'], time() + 50000, '/','.'.cfg::HTTP_HOST());
         setcookie ("hash", md5($_POST['login'].$_POST['password']), time() + 50000, '/','.'.cfg::HTTP_HOST());
         $_SESSION['login'] = $_POST['login'];

         header("Location: /adm/");
      }
   }
   private function logingOut() {
      unset($_SESSION['login']);
      setcookie("login", "", time() - 360000, '/','.'.cfg::HTTP_HOST());
      setcookie("hash", "", time() - 360000, '/','.'.cfg::HTTP_HOST());

      header("Location: /adm/");
   }

   /**
   *
   * Dashboard админ. панели
   *
   *
   */
   public function index() {
      $pg['title'] = "Панель администрирования";
      echo $this->content->design('adm','page-title',$pg);

      $sql = $this->db->select('delivery','COUNT(*) AS emails');
      $res = $this->db->getResult();
      $homepage['emails_in_db'] = $res[0]['emails'];

      echo $this->content->design('adm','homepage',$homepage);

      $this->__footer();
   }

  /**
   *
   * Работа с email рассылкой
   *
   *
   */
   public function delivery($params = false) {
      switch ($params['action']) {
         case 'add':
            $delivery_page  = 'delivery_add';
            $delivery_title = 'Добавление e-mail адреса';
            break;
         case 'show':
            $delivery_page  = 'delivery_show';
            $delivery_title = 'Список email адресов';
            break;
         default:
            $delivery_page = 'add';
      }

      $pg['title'] = 'Расылка email <small>'.$delivery_title.'</small>';
      echo $this->content->design('adm','page-title',$pg);

      $this->$delivery_page();

      $this->__footer();
   }

   private function delivery_add() {
      echo $this->content->design('adm','delivery-add');
   }

   private function delivery_show() {
      $params = new TimeParams();
      $params->format = 'd F Y года в H:i';
      $params->monthInflected = true;

      $sql = $this->db->sql('
                        SELECT *
                        FROM delivery
                        LIMIT 10
                     ');
      $res = $this->db->getResult();

      $tpl['list'] = '<tr>
                        <td><span class="badge">%s</span></td>
                        <td><code>%s</code></td>
                        <td>%s</td>
                        <td>%s</td>
                        <td>%s</td>
                        <td class="col-md-2 small">%s</td>
                     </tr>';
      $tpl['muted'] = '<span class="text-muted">%s</span>';

      foreach ($res as $list) {
         // ID
         $id    = $list['id'];
         // email
         $email = $list['email'];
         // Имя получателя
         $name  = (!empty($list['name']) ? $list['name'] : sprintf($tpl['muted'],'не указано'));
         // Дата добавления в БД
         if (!empty($list['date_reg'])) {
            $params->date = $list['date_reg'];
            $reg   = RUtils::dt()->ruStrFTime($params);
         } else
            $reg = '-';
         // Дата последней рассылки
         if (!empty($list['date_sent'])) {
            $params->date = $list['date_sent'];
            $sent  = RUtils::dt()->ruStrFTime($params);
         } else
            $sent = '-';
         // Заметки о email
         $note  = (!empty($list['note']) ? $list['note'] : sprintf($tpl['muted'],'не внесено'));

         @$cfg['list'] .= sprintf($tpl['list'],$id,$email,$name,$reg,$sent,$note);
      }

      echo $this->content->design('adm','delivery-show',$cfg);
   }

   public function tpl() {
      $body_cfg['url'] = cfg::URL;
      echo $this->content->design('email','001',$body_cfg);
   }

}
