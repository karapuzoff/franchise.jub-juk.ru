<?php
/**
 *
 * Класс с основными функциями
 *
 * @author Ivan Karapuzoff <ivan@karapuzoff.net>
 * @version 1.2
 *
 */

class master {
   /**
   *
   * Проверяет авторизацию пользователя
   *
   * @uses master::isLogin() для проверки
   * @return bool авторизован TRUE или нет FALSE
   *
   * @version 1.0
   *
   */
   static function isLogin() {
      if(isset($_SESSION['login']))
         return true;
      else
         return false;
   }

}
