<?php
/**
 *
 * Русский язык
 *
 * @uses constant(cfg::SITE_LANG.'::CONSTANT') для получения значения
 *
 * @author Ivan Karapuzoff <ivan@karapuzoff.net>
 * @version 0.2
 *
 */
  
class ru_RU {
# ========================================== Наши услуги =========================================================================== ->
   const MAIL_SUBJECT            = 'Запрос презентации франшизы',
         CONTACTUS_USERNAME      = 'Ваше имя',
         CONTACTUS_USEREMAIL     = 'Ваш e-mail',
         CONTACTUS_USERSUBJECT   = 'Тема',
         CONTACTUS_USERMESSAGE   = 'Сообщение',
         CONTACTUS_SENDBUTTON    = 'ОТПРАВИТЬ',
         SEND_FORM_OKTEXT        = '<h2>Ваша заявка успешно отправлена!</h2>';
# ========================================== END =================================================================================== ->
}