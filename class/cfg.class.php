<?php
/**
 *
 * Конфигурационный класс
 *
 * Основные настройки и методы
 *
 * @uses cfg::CONFIG_NAME для получения значения переменной конфигурационного файла
 *
 * @method http_host()
 *
 * @author Ivan Karapuzoff <ivan@karapuzoff.net>
 * @version 0.5
 *
 */
class cfg {
   const SITE_VERSION    = '1.0',
         SITE_LANG       = 'ru_RU',
         MAIL_ADDRESS_TO = 'yulya4886@mail.ru',
         URL             = 'franch.jub-jik.ru';
# ============================================= Параметры MySQL БД ================================================================= ->
   const DB_HOST         = 'localhost',            // Хост БД
         DB_NAME         = 'host1574_emails',      // Имя БД
         DB_USER         = 'host1574_emails',      // Пользователь БД
         DB_PASS         = 'Qwerty78',             // Пароль для доступа к БД
         DB_PREF         = '';                     // Префикс таблиц
# ============================================= Настройки почты ==================================================================== ->
   const SMTP_HOST       = 'ssl://smtp.yandex.ru', // Хост для отправки почты
         SMTP_PORT       = 465,                    // Порт для отправки почты
         SMTP_LOGIN      = 'franchise@jub-jik.ru', // Логин для авторизации
         SMTP_PASS       = 'ahfyibpf_jub-jik.ru',  // Пароль для авторизации
         SMTP_SENDER     = 'Франшиза Чуб-Чик';     // Отправитель
# ============================================= Простая авторизация ================================================================ ->
   const AUTH_LOGIN      = 'franchise',
         AUTH_PASS       = 'AhfyibpfXe,Xbr';
# ============================================= END ================================================================================ ->

  /**
   *
   * Имя хоста
   *
   * @uses cfg::http_host() для получения имени коренного домена
   *
   * @author Ivan Karapuzoff <ivan@karapuzoff.net>
   * @version 1.0
   *
   */
   public static function HTTP_HOST() {
      $host = implode('.', array_slice(explode('.', $_SERVER['SERVER_NAME']), -2));
   	return $host;
   }
}
