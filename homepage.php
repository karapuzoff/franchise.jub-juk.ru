<?php
class homepage {
  /**
   *
   * Конструктор класса
   *
   *
   */
   public function __construct() {
      $this->content = new template();
   }

  /**
   *
   * Сборка домашней страницы
   *
   * @version 2.0
   *
   */
   public function index() {      
      ### Заголовок страницы
      echo $this->content->design('main','header');
      
      echo $this->content->design('sections','intro');
      echo $this->content->design('sections','contacts-1',array('id'=>1));
      echo $this->content->design('sections','feature-1');
      echo $this->content->design('sections','contacts-1',array('id'=>2));
      
      $tabs['salon']  = $this->content->design('tabs','salon');
      $tabs['franch'] = $this->content->design('tabs','franch');
      $tabs['work']   = $this->content->design('tabs','work');
      echo $this->content->design('sections','tabs',$tabs);
      
      echo $this->content->design('sections','contacts-1',array('id'=>3));
      echo $this->content->design('sections','feature-3');
      echo $this->content->design('sections','contacts-1',array('id'=>4));
      echo $this->content->design('sections','plans');
      echo $this->content->design('sections','contacts-1',array('id'=>5));
      echo $this->content->design('sections','contacts',array('id'=>0));
      //echo $this->content->design('sections','contacts-1');
      //echo $this->content->design('sections','contacts-2');
   }
}
