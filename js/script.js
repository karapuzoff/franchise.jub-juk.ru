$(document).ready(function() {
    $("#franch-click").click(function() {
            var li = $(this).parent("li");
            var wch = this.getAttribute("href");
            //$("#franch").addClass("active");
            //$(li).siblings("li").removeClass("active");
            $(".tab-content div").removeClass("fade in active");
            $("#franch").addClass("active");
            return false; //don't change URL
        });
});

$(document).ready(function(){
   $("#intro").vegas({
      delay                : 10000,
      transitionDuration   : 2000,
      overlay              : true,
      transition           :'blur',
      slides: [
         { src: "/images/bg/girl-04.jpg" }
      ]
   });
});

var clock;

$(document).ready(function() {
   /*var clock;

   clock = $('.clock').FlipClock({
        language: 'ru',
        clockFace: 'Counter',
        autoStart: false,
        callbacks: {
         stop: function() {
            $('.message').html('Акция закончилась!')
         }
        }
    });

    clock.setTime(1234);
    clock.setCountdown(true);
    clock.start();/**/
    var clock_intro;
			$(document).ready(function() {
				// Grab the current date
				var currentDate = new Date();
				// Set some date in the future. In this case, it's always Jan 1
				var futureDate  = new Date("2015-09-20");
				// Calculate the difference in seconds between the future and current date
				var diff = futureDate.getTime() / 1000 - currentDate.getTime() / 1000;
				// Instantiate a coutdown FlipClock
				clock_intro = $('#clock-intro').FlipClock(diff, {
					language: 'ru',
               clockFace: 'DailyCounter',
					countdown: true
				});
            clock_buy = $('#clock-buy').FlipClock(diff, {
					language: 'ru',
               clockFace: 'DailyCounter',
					countdown: true
				});
			});

});

jQuery(window).load(function () {
    jQuery(".status").fadeOut();
    jQuery(".preloader").delay(500).fadeOut("slow");
});

wow = new WOW({ mobile: false }); wow.init();

jQuery(document).ready(function ($) {
    'use strict';

    $('.page-scroll').on('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    $(document).on('click', '.navbar-collapse.in', function (e) {
        if ($(e.target).is('a')) {
            $(this).collapse('hide');
        }
        return false;
    });

    $(window).scroll(function () {
        var secondFeature = $('#franchise').offset().top;
        var scroll = $(window).scrollTop();
        if (scroll >= 600) {
            $('.sticky-navigation').animate({"top": '0', "opacity": '1'}, 0);
        } else {
            $('.sticky-navigation').animate({"top": '-100', "opacity": '0'}, 0);
        }
        if (scroll >= secondFeature - 200) {
            $(".mobileScreen").css({'background-position': 'center top'});
        }
        return false;
    });

    var owl = $("#owl-carousel-works");
    owl.owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 2],
            [600, 3],
            [700, 3],
            [1000, 4],
            [1200, 4],
            [1400, 4],
            [1600, 4]
        ],
        navigation: false
    });

    $('#screenshots a').nivoLightbox({
        effect: 'fadeScale'
    });

    var owl = $("#owl-carousel-team");
    owl.owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 2],
            [600, 3],
            [700, 3],
            [1000, 4],
            [1200, 4],
            [1400, 4],
            [1600, 4]
        ],
        navigation: false
    });

    $("#owl-carousel-testimonials").owlCarousel({
        autoPlay: 2000,
        stopOnHover: true,
        navigation: false,
        paginationSpeed: 1000,
        goToFirstSpeed: 2000,
        singleItem: true,
        autoHeight: true,
        transitionStyle: "fade"
    });

    $('#numbers ul').appear(function () {
        $('#number1').animateNumber({number: 90}, 1500);
        $('#number2').animateNumber({number: 98}, 1500);
        $('#number3').animateNumber({number: 143}, 1500);
    }, {accX: 0, accY: -200});

$(".video-container").fitVids();

});

$(document).ready(function() {

	// process the form
   $('[id^=contactForm-]').submit(function(event) {

		var thisID   = '#' + this.id;
      var btnClone = $(this).find('[name^=submit]').clone();
      //console.log(btnClone);
		var formData = {
			'inputName' 	: $(this).find('input[name=inputName]').val(),
			'inputEmail' 	: $(this).find('input[name=inputEmail]').val(),
			'inputPhone'   : $(this).find('input[name=inputPhone]').val()
		};

		$(this).find('.form-group').removeClass('has-error');
		$(this).find('.help-block').remove();
		$(this).find('button').attr("disabled", true).html("Отправляю <i class=\"fa fa-lg fa-pulse fa-spinner\"></i>");

		$.ajax({
			type 		: 'POST',
			url 		: '/ajax/send.ajax.php',
			data 		: formData,
			dataType : 'json',
			encode 	: true
		}).done(function(data) {
				//console.log(data);

				if (!data.success) {
               //console.log('ERROR!!!');
               var errorClass = 'has-error bold txt';

					if (data.errors.uname) {
						//console.log('Name error');
                  $(thisID).find('[id^="name-group-"]').addClass(errorClass);
						$(thisID).find('[id^="name-group-"]').append('<span class="help-block">' + data.errors.uname + '</span>');
					}

					if (data.errors.phone) {
                  //console.log('Phone error');
						$(thisID).find('[id^="phone-group-"]').addClass(errorClass);
						$(thisID).find('[id^="phone-group-"]').append('<span class="help-block">' + data.errors.phone + '</span>');
					}

               if (data.errors.email) {
                  //console.log('Email error');
						$(thisID).find('[id^="email-group-"]').addClass(errorClass);
						$(thisID).find('[id^="email-group-"]').append('<span class="help-block">' + data.errors.email + '</span>');
					}

					//$(this).find('button').removeAttr("disabled").replaceWith(btnClone);
               $(thisID).find('[type^="submit"]').removeAttr("disabled").replaceWith(btnClone);
               //console.log(thisID);

				} else {
					//console.log('Success!');
               $(thisID).html('<div class="alert alert-success">' + data.message + '</div>');
					$(thisID).find('button').html('Отправлено');
				}

			}).fail(function(data) {
				//console.log(data);
			});

		event.preventDefault();
	});

});
