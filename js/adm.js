/*===================================================================================*/
/*  Добавление e-mail
/*===================================================================================*/
$(document).ready(function(){

   $( '#email-add-form' ).submit(function() {

      var action = "add";
      var email  = $('#email-add-email').val();
      var name   = $('#email-add-name').val();
      var note   = $('#email-add-note').val();

      $.post('/ajax/email.ajax.php', { action:action,email:email,name:name,note:note }, function(data) {
         var response = jQuery.parseJSON(data);
         $( '#email-add-response' ).html(response.message);
         $( '#email-add-error' ).html(response.error);
      }).fail(function() {
         alert( "Обновление не удалось" );
      });

      return false;

   });

});
