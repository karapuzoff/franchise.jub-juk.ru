<?php
function __autoload($class_name) {
   require_once '../class/' . $class_name . '.class.php';
}

if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])
   && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
   && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      
      $uname    = htmlspecialchars($_POST['inputName']);
      $phone   = htmlspecialchars($_POST['inputPhone']);
      $email   = $_POST['inputEmail'];
      
      $return['success'] = false;
      
      ### проверяем заполнено ли имя
      if (empty($uname)) {
         $return['errors']['uname'] = 'Имя нужно заполнить';
      }
      
      ### Проверяем тело сообщения
      if (empty($phone)) {
         $return['errors']['phone'] = 'Телефон нужно заполнить.';
      }
      
      ### Проверяем заполнен ли email и его валидность
      if (empty($email)) {
         $return['errors']['email'] = 'Email нужно заполнить.';
      } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
         $return['errors']['email'] = 'Вы указали некорректный e-mail адрес.';
      }
      
      ### Создаем разделитель
      $count = 30;
      $delim = null;
      for ($i=1;$i<=$count;$i++) {
         $delim .= '-';
      }
      
      if (!isset($return['errors'])) {
         $email_msg  = 'Автор: ' . $uname . "\r\n";
         $email_msg .= 'Email: ' . $email . "\r\n";
         $email_msg .= 'Телефон: ' . preg_replace("/[\r\n]+/", "\n", $phone);
         //$email_msg  = wordwrap($email_msg, 70, "\r\n");
         if (mail(cfg::MAIL_ADDRESS_TO, constant(cfg::SITE_LANG.'::MAIL_SUBJECT'), $email_msg)) {
            $return['success'] = true;
            $return['message'] = constant(cfg::SITE_LANG.'::SEND_FORM_OKTEXT');
         } else {
            $return['message'] = 'Не удается отправить письмо.';
         }
      }

      echo json_encode($return);
} else
   echo 'Не хорошо подсматривать...';
