<?php
function __autoload($class_name) {
   require_once '../class/' . $class_name . '.class.php';
}

if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])
   && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
   && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {/**/
      $db = new mysqlcrud();
      $db->connect();
      $content = new template();

      $email = htmlspecialchars($_POST['email']);
      $name  = htmlspecialchars($_POST['name']);
      $note  = htmlspecialchars($_POST['note']);

      ### проверяем заполнен ли email
      if (empty($email)) {
         $error['title'] = 'ОШИБКА';
         $error['note']  = 'Поле email обязательно для заполнения, без него невозможна отправка.';
         $return['message'] = $content->design('adm','block-error',$error);
      } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
         $error['title'] = 'ОШИБКА';
         $error['note']  = 'Введен Email неправильного формата. Правильный формат адреса: name@domain.ru';
         $return['message'] = $content->design('adm','block-error',$error);
      } else {
         $sql = $db->sql('
                  SELECT
                     id
                  FROM
                     delivery
                  WHERE
                     email = "'.$email.'"
                  ');
         $res = $db->getResult();
         if ($res == null) {
            $insert['email'] = $email;
            if (!empty($name))
               $insert['name'] = $name;
            if (!empty($note))
               $insert['note'] = $note;

            $sql = $db->insert('delivery',$insert);
            $res = $db->getResult();
            $ins = $db->getSql();

            // Отправка письма
            $objMail = new sendmailSMTP(cfg::SMTP_LOGIN,cfg::SMTP_PASS,cfg::SMTP_HOST,cfg::SMTP_SENDER,cfg::SMTP_PORT);
            $body_cfg['url'] = cfg::URL;
            $body = $content->design('email','001',$body_cfg);
            $objMail->send($email,'Презентация по Франшизе',$body);

            $success['title'] = 'УСПЕШНО';
            $success['note']  = 'Все успешно добавлено!<br /><code>'.$ins.'</code>';
            $return['message'] = $content->design('adm','block-success',$success);
         } else {
            $error['title'] = 'ОШИБКА';
            $error['note']  = 'Email адрес <strong>'.$email.'</strong> уже существует в базе данных.';
            $return['message'] = $content->design('adm','block-error',$error);
         }
      }

      echo json_encode($return);
} else
   echo 'Не хорошо подсматривать...';/**/
