<section id="salon-1">
   <div class="container">
      <div class="row">

         <div class="col-md-6 wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s">
            <div class="video-container">
               <div class="video">
                  <iframe width="640" height="390" src="https://www.youtube.com/embed/w15qFmrKyMQ" frameborder="0" allowfullscreen></iframe>
               </div>
            </div>
         </div>

         <div class="col-md-6 wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s">
            <div class="col-md-12 titleSection">
               <h2>Мы работаем с 2009 года.</h2>
               <p>
                  Наш первый салон открылся <span class="bold">29 августа 2009 года</span> в городе Курск.
                  Сегодня мы имеем три собственных салона, один из которых находится в соседнем регионе.
               </p>
               <div class="coloredLine"></div>
               
               <div class="space30"></div>
               
               <div class="col-md-4">
                  <a href="#" class="thumbnail">
                     <img src="/images/open/photo-01.jpg" class="img-responsive" alt="">
                  </a>
               </div>
               <div class="col-md-4">
                  <a href="#" class="thumbnail">
                     <img src="/images/open/photo-03.jpg" class="img-responsive" alt="">
                  </a>
               </div>
               <div class="col-md-4">
                  <a href="#" class="thumbnail">
                     <img src="/images/open/photo-02.jpg" class="img-responsive" alt="">
                  </a>
               </div>
               
               <div class="col-md-12 text-center submit">
                  <a class="page-scroll btn-new btn-bold" style="margin-top: 3rem" href="#contact">
                     <strong style="text-transform: uppercase">
                        <i class="fa fa-check fa-lg"></i>
                        Узнайте подробнее.
                     </strong>
                  </a>
               </div>
            </div>
         </div>
         
      </div>
   </div>
</section>

<section id="salon-2">
   <div class="container">
      <div class="row">
         
         <div class="col-md-6">
            <img src="/images/bubble-mission.png" class="wow bounceInLeft animated" data-wow-offset="120" data-wow-duration="1s" />
         </div>
         <div class="col-md-6 not-show">
            <img src="/images/bubble-mission-photo.png" class="wow bounceInRight animated" data-wow-offset="120" data-wow-duration="1.5s" />
         </div>
         <div class="col-md-6 not-show">
            <img src="/images/bubble-target-photo.png" class="wow bounceInLeft animated" data-wow-offset="120" data-wow-duration="1.5s" />
         </div>
         <div class="col-md-6">
            <img src="/images/bubble-target.png" class="wow bounceInRight animated" data-wow-offset="120" data-wow-duration="1s" />
         </div>
         
         <h2 class="text-center">Гарантия прибыли</h2>
         <div class="coloredLine"></div>
         
         <div class="space40"></div>
         
      </div>
   </div>
</section>

<section id="salon-3">
   <div class="container">
      
      <div class="row">
         <div class="col-md-5 wow bounceInUp animated pull-left" data-wow-offset="10" data-wow-duration="1.5s">
            <img src="/images/tabs/sad-baby.jpg" />
            <div class="space10"></div>
         </div>
         <div class="col-md-7 wow bounceInLeft animated animated" data-wow-offset="10" data-wow-duration="1s">
            <h3>
               Инфраструктура для детей в России все еще находится в зачаточном состоянии. Малышам негде поиграть, пока 
               родители нагружают тележку в супермаркете, а уж хорошо постричь ребенка - задача и вовсе неразрешимая.
            </h3>
         </div>
      </div>
      
      <div class="space40"></div>
      
      <div class="row">
         <div class="col-md-5 wow bounceInUp animated pull-right" data-wow-offset="10" data-wow-duration="1.5s">
            <img src="/images/tabs/increase.jpg" />
            <div class="space10"></div>
         </div>
         <div class="col-md-7 wow bounceInLeft animated" data-wow-offset="10" data-wow-duration="1s">
            <h3>
               В результате основных направлений семейной и демографической политики отмечены позитивные изменения: 
               показатель рождаемости увеличивается ежегодно в среднем на 7%.<br />
               Количество потенциальных клиентов увеличивается с каждым годом.
            </h3>
         </div>
      </div>
      
      <div class="space80"></div>
      
      <h2 class="text-center">Нашими Клиентами являются:</h2>
      
      <div class="space80"></div>
      
      <div class="row text-center mobile-only">
         
         <div class="col-xs-12">
            <h1>
               50%<br />
               <small>Дошкольники</small>
            </h1>
         </div>
         <div class="col-xs-12">
            <h1>
               25%<br />
               <small>Школьники</small>
            </h1>
         </div>
         <div class="col-xs-12">
            <h1>
               5%<br />
               <small>Дети от двух месяцев до полутора лет</small>
            </h1>
         </div>
         <div class="col-xs-12">
            <h1>
               20%<br />
               <small>Родители</small>
            </h1>
         </div>
         
      </div>
      
      <div class="desktop-only">
         
         <div class="row text-center">
            <div class="col-md-6">
               <span class="hero top">Дошкольники - 50%</span>
            </div>
            <div class="col-md-6">
               <span class="hero top">Около 5% - дети от двух месяцев до полутора лет</span>
            </div>
         </div>
         
         <div class="space30"></div>
         
         <div class="progress">
            <div class="progress-bar progress-bar-success" style="width: 50%">
               50%
            </div>
            <div class="progress-bar progress-bar-warning" style="width: 25%">
               25%
            </div>
            <div class="progress-bar progress-bar-danger" style="width: 5%">
               5%
            </div>
            <div class="progress-bar" style="width: 20%">
               20%
            </div>
         </div>
         
         <div class="row text-center">
            <div class="col-md-offset-6 col-md-3 h4">
               <span class="hero bottom">25% - школьники</span>
            </div>
            <div class="col-md-3 h4">
               <span class="hero bottom">20% - родители</span>
            </div>
         </div>
         
      </div>
      
   </div>
</section>
