<div class="container">
   <div class="col-md-12 text-center">
      <h1 class="text-center" style="padding-bottom: 30px">Начать сотрудничать с нами ОЧЕНЬ ЛЕГКО!</h1>
      
      <div style="font-size: 2rem">
         <div class="col-md-3">
            <img src="/images/icons/telephone-01.png" /><br />
            <mark><a class="page-scroll" href="#contact">ПОЗВОНИТЕ НАМ</a></mark><br />и заполните заявку.
            <div class="arrow-right"></div>
         </div>
         <!--div class="col-md-1"><i class="fa fa-arrow-right fa-3x"></i></div-->
         <div class="col-md-3">
            <img src="/images/icons/manager-01.png" /><br />
            Наш менеджер проведет<br />
            <mark>БЕСПЛАТНУЮ КОНСУЛЬТАЦИЮ</mark>.
            <div class="arrow-right"></div>
         </div>
         <!--div class="col-md-1"><i class="fa fa-arrow-right fa-3x"></i></div-->
         <div class="col-md-3">
            <img src="/images/icons/dogovor-01.png" /><br />
            Мы заключаем с Вами<br />
            <mark>ЛИЦЕНЗИОННЫЙ ДОГОВОР</mark>.
            <div class="arrow-right"></div>
         </div>
         <!--div class="col-md-1"><i class="fa fa-arrow-right fa-3x"></i></div-->
         <div class="col-md-3">
            <img src="/images/icons/start-01.png" /><br />
            Сразу после этого мы досылаем Вам все необходимые материалы и<br />
            <mark>НАЧИНАЕМ РАБОТАТЬ</mark>.
         </div>
      </div>
      
   </div>
</div>