<div class="container">

   <h2 class="text-center" style="padding-bottom: 30px;">В чем выгода нашей франшизы?</h2>
   <div class="col-md-6">
      
      <div class="features-right">
         <ul>
            
            <li class="wow fadeInLeft">
               <div class="features-ico"><i class="fa fa-clock-o yellow"></i></div>
               <div class="features-info">
                  <h4>Мы работаем с 2009 года на этом рынке.</h4>
                  <p>
                     Мы знаем все об индустрии детской красоты и поделимся своим опытом с Вами. Вы получаете право 
                     на использование бренда: название «Чуб-Чик», логотип, фирменный стиль.
                  </p>
               </div>
            </li>
            
            <li class="wow fadeInLeft">
               <div class="features-ico"><i class="fa fa-bolt red"></i></div>
               <div class="features-info">
                  <h4>Открытие – без проблем!</h4>
                  <p>
                     Только открываясь с нами, Вы избежите рисков проверок, штрафов и предписаний. У Вас будут четкие 
                     инструкции по способам уплаты налогов, соблюдению санитарных норм и других стандартов.
                  </p>
               </div>
            </li>
               
            <li class="wow fadeInLeft">
               <div class="features-ico"><i class="fa fa-building-o blue"></i></div>
               <div class="features-info">
                  <h4>Идеальное помещение.</h4>
                  <p>
                     Бесплатный подбор помещения (до оплаты паушального взноса) с учетом высокой проходимости и 
                     оптимальной стоимостью аренды.
                  </p>
               </div>
            </li>
               
            <li class="wow fadeInLeft">
               <div class="features-ico"><i class="fa fa-key blue2"></i></div>
               <div class="features-info">
                  <h4>Дизайн-проект «Под ключ»</h4>
                  <p>
                     Разрабатываем индивидуальный под ваше помещение дизайн-проект салона с эффективным использованием 
                     площади для размещения максимального количества точек дохода. Каждый кВ м должен работать!
                     <mark>Мы знаем как оптимизировать площадь салона.</mark>
                  </p>
               </div>
            </li>
            
            <li class="wow fadeInLeft">
               <div class="features-ico"><i class="fa fa-rub green"></i></div>
               <div class="features-info">
                  <h4>Полное оснащение по закупочным ценам</h4>
                  <p>Оснащение оборудованием и инструментами по оптовой цене!</p>
               </div>
            </li>
            
         </ul>
      </div>
      
   </div>
   <div class="col-md-6">
         
      <div class="features-right">
         <ul>
               
            <li class="wow fadeInRight">
               <div class="features-ico"><i class="fa fa-users pink"></i></div>
               <div class="features-info">
                  <h4>Проверенные методы найма и обучения персонала</h4>
                  <p>Предоставляем проверенные способы привлечения кандидатов и сценарии собеседований.</p>
               </div>
            </li>
               
            <li class="wow fadeInRight">
               <div class="features-ico"><i class="fa fa-trademark green"></i></div>
               <div class="features-info">
                  <h4>Маркетинг на 100%</h4>
                  <p>
                     Вы получаете рекламные площадки на нашем сайте и в группах соц.сетях.
                  </p>
               </div>
            </li>
               
            <li class="wow fadeInRight">
               <div class="features-ico"><i class="fa fa-graduation-cap yellow"></i></div>
               <div class="features-info">
                  <h4>Обучающий центр</h4>
                  <p>
                     Мы обучаем администраторов и мастеров по специально разработанной программе. Ваш персонал будет 
                     поэтапно обучатся стандартам клиентского сервиса и технологиям продаж и проходить стажировку у 
                     нашего бизнес-тренера.
                  </p>
               </div>
            </li>
               
            <li class="wow fadeInRight">
               <div class="features-ico"><i class="fa fa-life-ring blue3"></i></div>
               <div class="features-info">
                  <h4>Поддержка на всех этапах</h4>
                  <p>
                     Свои юристы, бухгалтера, служба технической поддержки, дизайнеры и другие специалисты – у Вас не 
                     болит голова по этим вопросам.
                  </p>
               </div>
            </li>
            
            <li class="wow fadeInRight">
               <div class="features-ico"><i class="fa fa-thumbs-o-up red"></i></div>
               <div class="features-info">
                  <h4>Опыт</h4>
                  <p>
                     Мы потратили 6 лет опыта и 4 млн. инвестиций, чтобы сформировать идеальную франшизу детской 
                     парикмахерской для Вас!
                  </p>
               </div>
            </li>
            
         </ul>
      </div>
                  
   </div>
   
   <div class="col-md-12 text-center clearfix">
      <a class="page-scroll btn-new btn-bold" style="margin-top: 3rem" href="#contact">
         <i class="fa fa-check fa-lg"></i>
         <strong style="text-transform: uppercase">Отправить заявку</strong> и получить 3 месяца работы <strong>без роялти</strong>.
      </a>
   </div>  
               
</div>