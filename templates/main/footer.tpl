<!--footer>
    <div class="container">
        <div class="col-md-12">
            <p>
               2015 <i class="fa fa-copyright"></i> Детская парикмахерская «Чуб-Чик».</p>
            <div class="space20"></div>
            <ul  class="circleSocial">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
            </ul>
        </div>
    </div>
</footer-->
<div class="clearfix"></div>

<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.easing.min.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/nivo-lightbox.min.js"></script>
<script src="/js/jquery.appear.js"></script>
<script src="/js/jquery.inview.js"></script>
<script src="/js/jquery.animateNumber.min.js"></script>
<script src="/js/jquery.fitvids.js"></script>
<script src="/js/wow.min.js"></script>
<script src="/js/script.js"></script>

</body>
</html>