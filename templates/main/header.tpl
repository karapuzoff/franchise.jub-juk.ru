<!doctype html>
<html lang="ru">
   <head>
      <meta charset="UTF-8">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="Ivan Karapuzoff [ivan@karapuzoff.net]">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      
      <title>Франшиза детской парикмахерской "Чуб Чик"</title>
        
      <link rel="stylesheet" href="/css/fonts/font-awesome/css/font-awesome.min.css">
      <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
      <link href="/css/bootstrap.min.css" rel="stylesheet">
      <link href="/css/owl-carousel/owl.carousel.css" rel="stylesheet">
      <link href="/css/owl-carousel/owl.theme.css" rel="stylesheet">
      <link href="/css/owl-carousel/owl.transitions.css" rel="stylesheet">
      <link href="/css/nivo-lightbox.css" rel="stylesheet">
      <link href="/css/nivo_themes/default/default.css" rel="stylesheet">
      <link href="/css/animate.min.css" rel="stylesheet">
      <link href="/js/vegas/vegas.min.css" rel="stylesheet">
      <link href="/css/flipclock.css" rel="stylesheet">
      <link href="/css/style.css" rel="stylesheet">
      
      <!--[if lt IE 9]>
         <script src="js/html5shiv.js"></script>
         <script src="js/respond.min.js"></script>
      <![endif]-->
      
      <script src="/js/jquery-1.11.2.min.js"></script>
      <script src="/js/modernizr.custom.min.js"></script>
      <script src="/js/flipclock.js"></script>
      <script src="/js/vegas/vegas.min.js"></script>
   </head>
   <body  id="page-top" data-spy="scroll" data-target=".navbar-custom">

        <header>
            <nav class="navbar navbar-custom navbar-top navbar-fixed-top sticky-navigation" role="navigation">
                <div class="container-fluid">
                    <a class="page-scroll btn-new btn-bold pull-right" href="#contact">Купить </a>
                    <div class="navbar-header page-scroll">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                            <i class="fa fa-bars"></i>
                        </button>

                        <a class="navbar-brand nav-logo" href="#page-top">
                            <img src="images/logo-xsmall.png" alt="mix"/>
                        </a>
                    </div>

                    <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a class="page-scroll" href="#page-top">В начало</a></li>
                            <li><a class="page-scroll" href="#franchise">Информация о нас</a></li>
                            <li>
                               <!--a class="page-scroll" href="#thirdFeature">Франчайзинг</a-->
                               <a id="franch-click" href="#franch" class="page-scroll" data-toggle="tab">Франчайзинг</a>
                            </li>
                            <li><a class="page-scroll" href="#plans">Финансовые условия</a></li>
                        </ul>
                    </div>

                </div>
            </nav>
        </header>