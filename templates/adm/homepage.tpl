<div class="row">

   <!--div class="col-lg-3 col-md-6">
      <div class="panel panel-primary">
         <div class="panel-heading">
            <div class="row">
               <div class="col-xs-3">
                  <i class="fa fa-shopping-cart fa-5x"></i>
               </div>
               <div class="col-xs-9 text-right">
                  <div class="huge">{new_orders}</div>
                  <div>Новых заказов</div>
               </div>
            </div>
         </div>
         <a href="/cc/orders/status/new/">
            <div class="panel-footer">
               <span class="pull-left">Смотреть заказы</span>
               <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
               <div class="clearfix"></div>
            </div>
         </a>
      </div>
   </div>

   <div class="col-lg-3 col-md-6">
      <div class="panel panel-green">
         <div class="panel-heading">
            <div class="row">
               <div class="col-xs-3">
                  <i class="fa fa-users fa-5x"></i>
               </div>
               <div class="col-xs-9 text-right">
                  <div class="huge">{users_total}</div>
                  <div>Пользователей</div>
               </div>
            </div>
         </div>
         <a href="#">
            <div class="panel-footer">
               <span class="pull-left">Смотреть</span>
               <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
               <div class="clearfix"></div>
            </div>
         </a>
      </div>
   </div>

   <div class="col-lg-3 col-md-6">
      <div class="panel panel-red">
         <div class="panel-heading">
            <div class="row">
               <div class="col-xs-3">
                  <i class="fa fa-cubes fa-5x"></i>
               </div>
               <div class="col-xs-9 text-right">
                  <div class="huge">{items_total}</div>
                  <div>Товаров в базе</div>
               </div>
            </div>
         </div>
         <a href="/cc/priceupdate/">
            <div class="panel-footer">
               <span class="pull-left">Обновить прайс-лист</span>
               <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
               <div class="clearfix"></div>
            </div>
         </a>
      </div>
   </div-->

   <div class="col-lg-3 col-md-6">
      <div class="panel panel-yellow">
         <div class="panel-heading">
            <div class="row">
               <div class="col-xs-3">
                  <i class="fa fa-envelope fa-5x"></i>
               </div>
               <div class="col-xs-9 text-right">
                  <div class="huge">{emails_in_db}</div>
                  <div>e-mail в базе</div>
               </div>
            </div>
         </div>
         <a href="/adm/delivery/action/add/">
            <div class="panel-footer">
               <span class="pull-left">Добавить e-mail</span>
               <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
               <div class="clearfix"></div>
            </div>
         </a>
      </div>
   </div>

</div>
