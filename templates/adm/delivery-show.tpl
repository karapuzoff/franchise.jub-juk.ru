<div class="panel panel-default">
   <div class="panel-heading">
      Последние добавленные
   </div>

   <div class="panel-body">
      <div class="table-responsive">
         <table class="table table-striped">

            <thead>
               <tr>
                  <th>#</th>
                  <th>email адрес</th>
                  <th>Имя</th>
                  <th>Дата добавления</th>
                  <th>Дата очередной отправки</th>
                  <th>Заметки</th>
               </tr>
            </thead>

            <tbody>

               {list}

            </tbody>

         </table>
      </div>
   </div>
</div>
