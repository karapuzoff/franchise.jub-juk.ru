   <div class="navbar-default sidebar" role="navigation">
      <div class="sidebar-nav navbar-collapse">
         <ul class="nav" id="side-menu">

            <li class="sidebar-search">
               <h4 class="input-group">
                  <a href="/"><i class="fa fa-arrow-left fa-lg"></i>
                     обратно к Франшизе
                  </a>
               </h4>
            </li>

            <li><a href="/adm/"><i class="fa fa-dashboard"></i>
               Главная панель</a></li>

            <li>
               <a href="#"><i class="fa fa-envelope-o fa-lg"></i>
                  Рассылка e-mail
                  <span class="fa arrow"></span>
               </a>
               <ul class="nav nav-second-level">
                  <li><a href="/adm/delivery/action/add/"><i class="fa fa-plus-circle"></i> Добавить e-mail</a></li>
                  <li><a href="/adm/delivery/action/show/"><i class="fa fa-list-ol"></i> Список адресов</a></li>
               </ul>
            </li>

         </ul>
      </div>
   </div>
</nav>
