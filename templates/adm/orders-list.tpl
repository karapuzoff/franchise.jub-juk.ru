<div class="row">
   <div class="col-sm-12">
   
      <div class="panel panel-default">
         
         <div class="panel-heading">
            <!--span class="pull-right lead">
               {status}
            </span>
            <div class="panel-title">
               <code><abbr title="{was_created}" class="lead">{id}</abbr></code>
               <i class="fa fa-clock-o"></i> {was_created}
            </div-->
            
            <div class="row">
               
               <div class="col-sm-12">
                  <div class="col-lg-1 col-sm-12">
                     <i class="fa fa-5x fa-user"></i>
                  </div>
                  <div class="col-lg-3 col-sm-12 lead">
                     <dd>{order_fio}</dd>
                     <dd><code>{order_phone}</code></dd>
                     <dd class="small"><i class="fa fa-map-marker"></i> {user_city}</dd>
                     <small><i class="fa fa-clock-o"></i> {date_post}</small>
                  </div>
                  <div class="col-lg-4 col-sm-12">
                     {user_info}
                  </div>
                  <div class="col-lg-4 col-sm-12">
                     {note_user}
                  </div>
                  
               </div>
               
            </div>
            
            <div class="col-sm-12">
               <div class="progress{order_bar_striped}">
                  <div class="progress-bar progress-bar-{order_status_style}" style="width: {order_status_stage}%">
                     <span>{order_status_name}</span>
                  </div>
               </div>
            </div>
            
         </div>
         
         <div class="panel-body">
            
            <div class="col-sm-12">
               <table class="table table-striped">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th>Группа товара</th>
                        <th>Товарная позиция</th>
                        <th>Количество</th>
                        <th>Цена</th>
                        <th>Итого</th>
                     </tr>
                  </thead>
                  <tbody>
                     {order_cart}
                  </tbody>
               </table>
               
               <h3 class="pull-right">{order_total}</h3>
               
            </div>
         </div>
         
         <div class="panel-footer">
            <div class="row">
               <div class="col-sm-12">
               
                  <div class="pull-right">
                     <div class="btn-group" role="group">
                        <button type="button" class="btn btn-danger"><i class="fa fa-lg fa-trash-o"></i></button>
                     </div>
                     
                     <div class="btn-group" role="group">
                        <button type="button" class="btn btn-success">Заказ подтвержден оператором <i class="fa fa-lg fa-check"></i></button>
                     </div>
                  </div>
               
               </div>
            </div>
         </div>
         
      </div>
   
   </div>
</div>
