<div class="row">
   <div class="col-lg-12">
      <div class="panel panel-default">

         <div class="panel-heading">
            Добавить новый email-адрес
         </div>

         <div class="panel-body">
            <div class="row">

               <div class="col-lg-6 col-xs-12">
                  <form role="form" name="email-add-form" id="email-add-form">

                     <div class="form-group">
                        <label>Адрес email</label>
                        <input type="email" class="form-control" id="email-add-email" name="email-add-email" placeholder="name@domain.ru" />
                        <p class="help-block">Новый email адрес для добавления в базу.</p>
                     </div>

                     <div class="form-group">
                        <label>Имя</label>
                        <input type="text" class="form-control" id="email-add-name" name="email-add-name" placeholder="Иванова Екатерина" />
                        <p class="help-block">Имя пользователя email</p>
                     </div>

                     <div class="form-group">
                        <label>Примечание к email-адресу</label>
                        <textarea class="form-control" id="email-add-note" name="email-add-note" rows="3"></textarea>
                     </div>

                     <div class="pull-right">
                        <button type="reset" class="btn btn-danger" id="email-add-submit"><i class="fa fa-times fa-lg"></i> Сбросить форму</button>
                        <button type="submit" class="btn btn-success"><i class="fa fa-plus fa-lg"></i> Добавить адрес</button>
                     </div>

                  </form>
               </div>

               <div class="col-lg-6 col-xs-12">
                  <div id="email-add-response"></div>
               </div>

            </div>
         </div>
      </div>
   </div>
</div>
