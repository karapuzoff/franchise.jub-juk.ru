<div class="container">
   <div class="row">
      <div class="col-md-4 col-md-offset-4">
         <div class="login-panel panel panel-default">
            <div class="panel-heading">
               <h3 class="panel-title">Пожалуйста, войдите</h3>
            </div>
            <div class="panel-body">
               <form method="POST" role="form">
                  <fieldset>
                     <div class="form-group">
                        <input type="login" class="form-control" placeholder="Имя пользователя" name="login" autofocus="">
                     </div>
                     <div class="form-group">
                        <input type="password" class="form-control" placeholder="Пароль" name="password" />
                     </div>
                     <input type="submit" class="btn btn-lg btn-success btn-block" />
                  </fieldset>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
