<!DOCTYPE html>
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

   <title>Спасибо за проявленный интерес к нашему бизнесу</title>

   <style type="text/css">
      /* Client-specific Styles */
      #outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
      body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
      /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
      .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
      .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.*/
      #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
      img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
      a img {border:none;}
      .image_fix {display:block;}
      p {margin: 0px 0px !important;}
      table td {border-collapse: collapse;}
      table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
      a {color: #0a8cce;text-decoration: none;text-decoration:none!important;}
      /*STYLES*/
      table[class=full] { width: 100%; clear: both; }
      hr {
         border: none;
         color: #ccc;
         background-color: #ccc; /* Firefox и Opera */
         height: 1px;
      }
      .bordered {
         border-top: 1px solid #e8e8e8;
      }
      .lined {
         background: #f8f8f8;
      }
      /*IPAD STYLES*/
      @media only screen and (max-width: 640px) {
         a[href^="tel"], a[href^="sms"] {
            text-decoration: none;
            color: #0a8cce; /* or whatever your want */
            pointer-events: none;
            cursor: default;
         }
         .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
            text-decoration: default;
            color: #0a8cce !important;
            pointer-events: auto;
            cursor: default;
         }
         table[class=devicewidth] {width: 440px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
         img[class=banner] {width: 440px!important;height:220px!important;}
         img[class=colimg2] {width: 440px!important;height:220px!important;}
      }
      /*IPHONE STYLES*/
      @media only screen and (max-width: 480px) {
         a[href^="tel"], a[href^="sms"] {
            text-decoration: none;
            color: #0a8cce; /* or whatever your want */
            pointer-events: none;
            cursor: default;
         }
         .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
            text-decoration: default;
            color: #0a8cce !important;
            pointer-events: auto;
            cursor: default;
         }
         table[class=devicewidth] {width: 280px!important;text-align:center!important;}
         table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
         img[class=banner] {width: 280px!important;height:140px!important;}
         img[class=colimg2] {width: 280px!important;height:140px!important;}
         td[class=mobile-hide]{display:none!important;}
         td[class="padding-bottom25"]{padding-bottom:25px!important;}

      }
   </style>
</head>
<body>

<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="preheader" >
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>

                              <tr>
                                 <td width="100%" height="10"></td>
                              </tr>

                              <tr>
                                 <td>
                                    <table width="100" align="left" border="0" cellpadding="0" cellspacing="0">
                                       <tbody>
                                          <tr>
                                             <td align="left" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 14px;color: #666666" st-content="viewonline" class="mobile-hide">
                                                <a href="http://franch.jub-jik.ru" style="text-decoration: none; color: #666666">Чуб-Чик</a>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table width="100" align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                       <tbody>
                                          <tr>
                                             <td width="30" height="30" align="right">
                                                <div class="imgpop">
                                                   <a target="_blank" href="#">
                                                   <img src="http://{url}/images/email/001/facebook.png" alt="" border="0" width="30" height="30" style="display:block; border:none; outline:none; text-decoration:none;">
                                                   </a>
                                                </div>
                                             </td>
                                             <td align="left" width="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                             <td width="30" height="30" align="center">
                                                <div class="imgpop">
                                                   <a target="_blank" href="#">
                                                   <img src="http://{url}/images/email/001/twitter.png" alt="" border="0" width="30" height="30" style="display:block; border:none; outline:none; text-decoration:none;">
                                                   </a>
                                                </div>
                                             </td>
                                             <td align="left" width="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
                                             <td width="30" height="30" align="center">
                                                <div class="imgpop">
                                                   <a target="_blank" href="#">
                                                   <img src="http://{url}/images/email/001/linkedin.png" alt="" border="0" width="30" height="30" style="display:block; border:none; outline:none; text-decoration:none;">
                                                   </a>
                                                </div>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td width="100%" height="10"></td>
                              </tr>
                              <!-- Spacing -->
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of preheader -->
<!-- Start of header -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                              <tr>
                                 <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td>
                                    <!-- logo -->
                                    <table width="140" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                       <tbody>
                                          <tr>
                                             <td width="169" height="45" align="center">
                                                <div class="imgpop">
                                                   <h2 style="font-family: Helvetica, arial, sans-serif; font-size: 30px; color: #333333; text-align:center; line-height: 30px;" st-title="fulltext-heading">
                                                      Парикмахерская<br />для детей
                                                   </h2>
                                                   <a target="_blank" href="#">
                                                      <img src="http://{url}/images/logo-mid-2.png" alt="" border="0" width="400" height="178" style="display:block; border:none; outline:none; text-decoration:none;">
                                                   </a>
                                                </div>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!-- end of logo -->
                                 </td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                              </tr>
                              <!-- Spacing -->
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of Header -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
   <tbody>
      <tr>
         <td>
            <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
               <tbody>
                  <tr>
                     <td align="center" height="20" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of seperator -->
<!-- Start Full Text -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="full-text">
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                              <tr>
                                 <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td>
                                    <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                       <tbody>
                                          <!-- Title -->
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 30px; color: #333333; text-align:center; line-height: 30px;" st-title="fulltext-heading">
                                                Уважаемый коллега!
                                             </td>
                                          </tr>
                                          <!-- End of Title -->
                                          <!-- spacing -->
                                          <tr>
                                             <td width="100%" height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                          </tr>
                                          <!-- End of spacing -->
                                          <!-- content -->
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #666666; text-align:left; line-height: 25px;" st-content="fulltext-content">
                                                <p style="padding-bottom: 10px;">
                                                   Предлагаю Вам партнерство в интересном, перспективном и динамично развивающемся бизнесе – парикмахерской для детей. Несмотря на все успехи в развитии потребительского рынка, инфраструктура для детей у нас все еще находится в зачаточном состоянии. Повсеместно обнаруживаются провалы и дыры: очереди в детских поликлиниках, острая нехватка стоматологии «без боли» и с дружелюбными врачами… Малышам негде поиграть, пока родители нагружают тележку в супермаркете, а уж хорошо постричь ребенка — задача и вовсе неразрешимая.
                                                </p>
                                                <p style="padding-bottom: 10px;">
                                                   Парикмахерская для детей «Чуб-Чик» успешно функционирует с 2009 года! Успешность бизнеса сети парикмахерских для детей «Чуб-Чик» подтверждается тем, что у нас ни один салон не закрылся! «Чуб-Чик» пользуется большой популярностью и высокой посещаемостью. Только за один месяц один салон принимает более тысячи Клиентов.
                                                </p>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td width="100%" height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                          </tr>
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 30px; color: #333333; text-align:center; line-height: 30px;" st-title="fulltext-heading">
                                                Не верите?
                                             </td>
                                          </tr>
                                          <tr>
                                             <td width="100%" height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                          </tr>
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #666666; text-align:left; line-height: 25px;" st-content="fulltext-content">
                                                <p style="padding-bottom: 10px;">
                                                   Мы готовы показать реальные цифры доходов сети парикмахерских с годовым оборотом
                                                </p>
                                                <p style="padding-bottom: 10px;">
                                                   Главная идея нашего бизнеса - научить маленького Клиента любить свою внешность, ухаживать за собой, меняться, преображаться и не боятся быть ярким. Дать возможность ребенку почувствовать себя настоящим героем, вокруг которого происходит основное действие.
                                                </p>

                                             </td>
                                          </tr>
                                          <!-- End of content -->
                                          <tr>
                                             <td>
                                                <!-- Start of main-banner -->
                                                <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner" style="margin: 25px 0">
                                                   <tbody>
                                                      <tr>
                                                         <td>
                                                            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                                                               <tbody>
                                                                  <tr>
                                                                     <td width="100%">
                                                                        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                                                                           <tbody>
                                                                              <tr>
                                                                                 <td align="center" st-image="banner-image">
                                                                                    <div class="imgpop">
                                                                                       <a target="_blank" href="#"><img width="600" border="0" height="300" alt="" border="0" style="display:block; border:none; outline:none; text-decoration:none;" src="http://{url}/images/email/salon-01.jpg" class="banner"></a>
                                                                                    </div>
                                                                                 </td>
                                                                              </tr>
                                                                           </tbody>
                                                                        </table>
                                                                     </td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                                <!-- End of main-banner -->
                                             </td>
                                          </tr>
                                          <tr>
                                             <td width="100%" height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                          </tr>
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 30px; color: #333333; text-align:center; line-height: 30px;" st-title="fulltext-heading">
                                                В чем выгода нашей франшизы?
                                             </td>
                                          </tr>
                                          <tr>
                                             <td width="100%" height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                          </tr>
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #666666; text-align:left; line-height: 25px;" st-content="fulltext-content">
                                                <p style="padding-bottom: 10px;">
                                                   <strong>Мы работаем с 2009 года на этом рынке.</strong> Мы знаем практически все об индустрии красоты и поделимся своим опытом с Вами. Вы получаете право на использование бренда: название «Чуб-Чик», логотип, фирменный стиль.
                                                </p>
                                                <p style="padding-bottom: 10px;">
                                                   Вы получаете <strong>схемы эффективных рекламных кампаний</strong> и список не эффективных.
                                                </p>
                                                <p style="padding-bottom: 10px;">
                                                   <strong>Альбом причесок и стрижек</strong>, сделанные нашими мастерами, который крайне необходим на начальном этапе деятельности.
                                                </p>
                                                <p style="padding-bottom: 10px;">
                                                   <strong>Маркетинговая поддержка.</strong> Вы получаете рекламные площадки на нашем сайте и в группах соц.сетях.
                                                </p>
                                                <p style="padding-bottom: 10px;">
                                                   <strong>Пошаговые инструкции</strong>. Вам не нужно думать что и как делать – мы все продумали за Вас:
                                                   <ol style="font-size: 14px;">
                                                      <li>
                                                         <strong>Правила открытия салона и взаимодействия с контролирующими органами.</strong><br />
                                                         Только открываясь с нами, Вы избежите рисков проверок, штрафов и предписаний. У Вас будут четкие инструкции по способам уплаты налогов, соблюдению санитарных норм и других стандартов.
                                                      </li>
                                                      <li>
                                                         <strong>Подбор помещения с учетом высокой проходимости и оптимальной стоимости аренды.</strong>
                                                      </li>
                                                      <li>
                                                         <strong>Индивидуальный дизайн-проект салона с эффективным использованием площади для размещения  максимального количества точек дохода.</strong><br />
                                                         Каждый квадратный метр должен работать. Мы знаем, как оптимизировать площадь салона.
                                                      </li>
                                                      <li>
                                                         <strong>Оснащение салона необходимым оборудованием  и инструментами по оптовой цене.</strong>
                                                      </li>
                                                      <li>
                                                         <strong>Разработанные методы поиска и отбора персонала.</strong><br />
                                                         Предоставляем проверенные способы привлечения кандидатов и сценарии собеседований.
                                                      </li>
                                                      <li>
                                                         <strong>Построение работы с персоналом.</strong><br />
                                                         Для каждого сотрудника прописана четкая должностная инструкция и  механизм начисления заработной платы.
                                                      </li>
                                                   </ol>
                                                </p>
                                                <p style="padding-bottom: 10px;">
                                                   <strong>Обучающий центр.</strong> Мы обучаем администраторов и мастеров по специально разработанной программе. Ваш персонал будет поэтапно обучатся стандартам клиентского сервиса и технологиям продаж и проходить стажировку у нашего бизнес-тренера.
                                                </p>
                                                <p style="padding-bottom: 10px;">
                                                   <strong>Поддержка на всех этапах.</strong> Свои юристы, бухгалтера, служба технической поддержки, дизайнеры и другие специалисты – у Вас не болит голова по этим вопросам.
                                                </p>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                              </tr>
                              <!-- Spacing -->
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- end of full text -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="banner" style="margin: 10px 0">
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                           <tbody>
                              <tr>
                                 <td align="center" st-image="banner-image">
                                    <div class="imgpop">
                                       <a target="_blank" href="#"><img width="600" border="0" height="300" alt="" border="0" style="display:block; border:none; outline:none; text-decoration:none;" src="http://{url}/images/email/salon-02.jpg" class="banner"></a>
                                    </div>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- Start of seperator -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
   <tbody>
      <tr>
         <td>
            <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
               <tbody>
                  <tr>
                     <td align="center" height="30" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td width="550" align="center" height="1" bgcolor="#d1d1d1" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td align="center" height="30" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of seperator -->
<!-- 3 Start of Columns -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <tr>
                                 <td>
                                    <!-- col 1 -->
                                    <table width="186" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                       <tbody>
                                          <!-- image 2 -->
                                          <tr>
                                             <td width="100%" align="center" class="devicewidth">
                                                <img src="http://{url}/images/email/001/icon1.png" alt="" border="0" width="50" height="50" style="display:block; border:none; outline:none; text-decoration:none;">
                                             </td>
                                          </tr>
                                          <!-- end of image2 -->
                                          <tr>
                                             <td>
                                                <!-- start of text content table -->
                                                <table width="186" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                   <tbody>
                                                      <!-- Spacing -->
                                                      <tr>
                                                         <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                      </tr>
                                                      <!-- Spacing -->
                                                      <!-- title2 -->
                                                      <tr>
                                                         <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #666666; text-align:center; line-height: 24px;" st-title="3col-title1">
                                                            Лайт
                                                         </td>
                                                      </tr>
                                                      <!-- end of title2 -->
                                                      <!-- Spacing -->
                                                      <tr>
                                                         <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                      </tr>
                                                      <!-- Spacing -->
                                                      <!-- content2 -->
                                                      <tr>
                                                         <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #889098; text-align:center; line-height: 24px;" st-content="3col-content1">
                                                            <hr />
                                                            Помещение – <b>35-40 кв.м.</b><br />
                                                            2 детских кресла + 1 взрослое (только парикмахерский зал).
                                                            <hr />
                                                            <b>Инвестиции:</b><br />от 245 000 до 295 000 руб.
                                                            <hr />
                                                            <b>Паушальный взнос:</b><br />90 000 руб.
                                                            <hr />
                                                            <b>Роялти:</b><br />6 000 руб.
                                                         </td>
                                                      </tr>
                                                      <!-- end of content2 -->
                                                      <!-- Spacing -->
                                                      <tr>
                                                         <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                      </tr>
                                                      <!-- Spacing -->
                                                      <!-- read more -->
                                                      <tr>
                                                         <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #0a8cd8;  text-align:center; line-height: 20px;" st-title="3col-readmore1" class="padding-bottom25">
                                                            <a href="#" style="text-decoration:none; color: #0a8cd8; ">Подробности</a>
                                                         </td>
                                                      </tr>
                                                      <!-- end of read more -->
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                          <!-- end of text content table -->
                                       </tbody>
                                    </table>
                                    <!-- spacing -->
                                    <table width="20" align="left" border="0" cellpadding="0" cellspacing="0" class="removeMobile">
                                       <tbody>
                                          <tr>
                                             <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!-- end of spacing -->
                                    <!-- col 2 -->
                                    <table width="186" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                       <tbody>
                                          <!-- image 2 -->
                                          <tr>
                                             <td width="100%" align="center" class="devicewidth">
                                                <img src="http://{url}/images/email/001/icon1.png" alt="" border="0" width="50" height="50" style="display:block; border:none; outline:none; text-decoration:none;">
                                             </td>
                                          </tr>
                                          <!-- end of image2 -->
                                          <tr>
                                             <td>
                                                <!-- start of text content table -->
                                                <table width="186" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                   <tbody>
                                                      <!-- Spacing -->
                                                      <tr>
                                                         <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                      </tr>
                                                      <!-- Spacing -->
                                                      <!-- title2 -->
                                                      <tr>
                                                         <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #666666; text-align:center; line-height: 24px;" st-title="3col-title2">
                                                            Стандарт
                                                         </td>
                                                      </tr>
                                                      <!-- end of title2 -->
                                                      <!-- Spacing -->
                                                      <tr>
                                                         <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                      </tr>
                                                      <!-- Spacing -->
                                                      <!-- content2 -->
                                                      <tr>
                                                         <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #889098; text-align:center; line-height: 24px;" st-content="3col-content2">
                                                            <hr />
                                                            Помещение – <b>50-60 кв.м.</b><br />
                                                            3 детских кресла + 2 взрослых + маникюрный кабинет.
                                                            <hr />
                                                            <b>Инвестиции:</b><br />от 550 000 до 580 000 руб.
                                                            <hr />
                                                            <b>Паушальный взнос:</b><br />140 000 руб.
                                                            <hr />
                                                            <b>Роялти:</b><br />10 000 руб.
                                                         </td>
                                                      </tr>
                                                      <!-- end of content2 -->
                                                      <!-- Spacing -->
                                                      <tr>
                                                         <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                      </tr>
                                                      <!-- /Spacing -->
                                                      <!-- read more -->
                                                      <tr>
                                                         <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #0a8cd8;  text-align:center; line-height: 20px;" st-title="3col-readmore2" class="padding-bottom25">
                                                            <a href="#" style="text-decoration:none; color: #0a8cd8; ">Узнать больше</a>
                                                         </td>
                                                      </tr>
                                                      <!-- end of read more -->
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                          <!-- end of text content table -->
                                       </tbody>
                                    </table>
                                    <!-- end of col 2 -->
                                    <!-- spacing -->
                                    <table width="1" align="left" border="0" cellpadding="0" cellspacing="0" class="removeMobile">
                                       <tbody>
                                          <tr>
                                             <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <!-- end of spacing -->
                                    <!-- col 3 -->
                                    <table width="186" align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                       <tbody>
                                          <!-- image3 -->
                                          <tr>
                                             <td width="100%" align="center" class="devicewidth">
                                                <img src="http://{url}/images/email/001/icon1.png" alt="" border="0" width="50" height="50" style="display:block; border:none; outline:none; text-decoration:none;">
                                             </td>
                                          </tr>
                                          <!-- end of image3 -->
                                          <tr>
                                             <td>
                                                <!-- start of text content table -->
                                                <table width="186" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                   <tbody>
                                                      <!-- Spacing -->
                                                      <tr>
                                                         <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                      </tr>
                                                      <!-- Spacing -->
                                                      <!-- title -->
                                                      <tr>
                                                         <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #666666; text-align:center; line-height: 24px;" st-title="3col-title3">
                                                            Премиум
                                                         </td>
                                                      </tr>
                                                      <!-- end of title -->
                                                      <!-- Spacing -->
                                                      <tr>
                                                         <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                      </tr>
                                                      <!-- Spacing -->
                                                      <!-- content -->
                                                      <tr>
                                                         <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #889098; text-align:center; line-height: 24px;" st-content="3col-content3">
                                                            <hr />
                                                            Помещение: <b>70-100 кв.м.</b><br />
                                                            4 детских кресла + 3 взрослых + маникюрный кабинет + тату-кабинет.
                                                            <hr />
                                                            <b>Инвестиции:</b><br />от 750 000 до 940 000 руб.
                                                            <hr />
                                                            <b>Паушальный взнос:</b><br />350 000 руб.
                                                            <hr />
                                                            <b>Роялти:</b><br />25 000 руб.
                                                         </td>
                                                      </tr>
                                                      <!-- end of content -->
                                                      <!-- Spacing -->
                                                      <tr>
                                                         <td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                                      </tr>
                                                      <!-- Spacing -->
                                                      <!-- read more -->
                                                      <tr>
                                                         <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #0a8cce;  text-align:center; line-height: 20px;" st-title="3col-readmore3">
                                                            <a href="#" style="text-decoration:none; color: #0a8cce; ">Выбрать план</a>
                                                         </td>
                                                      </tr>
                                                      <!-- end of read more -->
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                          <!-- end of text content table -->
                                       </tbody>
                                    </table>
                                 </td>
                                 <!-- spacing -->
                                 <!-- end of spacing -->
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- end of 3 Columns -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
   <tbody>
      <tr>
         <td>
            <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
               <tbody>
                  <tr>
                     <td align="center" height="30" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td width="550" align="center" height="1" bgcolor="#d1d1d1" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td align="center" height="30" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of seperator -->

<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
   <tbody>
      <tr>
         <td>
            <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
               <thead style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #889098; text-align:center; line-height: 24px;">
                  <tr>
                     <td>&nbsp;</td>
                     <td style="padding: 5px;"><h3>Лайт</h3></td>
                     <td style="padding: 5px;"><h3>Стандарт</h3></td>
                     <td style="padding: 5px;"><h3>Премиум</h3></td>
                  </tr>
               </thead>
               <tbody style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #889098; text-align:center; line-height: 24px;">
                  <tr class="bordered">
                     <td style="padding: 5px; text-align:left;">Право на использование бренда "Чуб-Чик"</td>
                     <td>+</td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered lined">
                     <td style="padding: 5px; text-align:left;">Бренд бук</td>
                     <td>+</td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered">
                     <td style="padding: 5px; text-align:left;">Бизнес бук. Составление индивидуального бизнес плана.</td>
                     <td>+</td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered lined">
                     <td style="padding: 5px; text-align:left;">Франчайзи бук</td>
                     <td>+</td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered">
                     <td style="padding: 5px; text-align:left;">Основные этапы запуска (открытия) салона, технические требования.</td>
                     <td>+</td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered lined">
                     <td style="padding: 5px; text-align:left;">Полное юридическое сопровождение
Перечень типичных документов, необходимых для открытия и функционирования парикмахерской с указанием органа или организации, обеспечивающих получение таких документов. Получение необходимых документов и подписание договоров</td>
                     <td>+</td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered">
                     <td style="padding: 5px; text-align:left;">Дизайн проект помещения «под ключ». Решение технических вопросов (ремонт помещения, проведение коммуникаций и т.д.</td>
                     <td></td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered lined">
                     <td style="padding: 5px; text-align:left;">Обучение и внедрение стандартов контроля салона. Способы контроля; аспекты, характеристики, процессы и результаты, подлежащие контролю; порядок проведения плановых и внеплановых мероприятий с целью контроля салона</td>
                     <td></td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered">
                     <td style="padding: 5px; text-align:left;">Система подбора, подготовки и обучения кадров. Адаптация и развитие персонала. Порядок обучения, стажировка персонала. Подбор персонала. Порядок осуществления поиска, отбора, анализа и оформления сотрудников на работу, особенности подбора персонала для салона.</td>
                     <td></td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered lined">
                     <td style="padding: 5px; text-align:left;">Единые стандарты для каждой штатной единицы</td>
                     <td></td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered">
                     <td style="padding: 5px; text-align:left;">Идеальная система мотивации для персонала. Порядок поощрения, премирования, мотивации, а также наложение дисциплинарных взысканий на сотрудников салона</td>
                     <td></td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered lined">
                     <td style="padding: 5px; text-align:left;">Возможность обучения мастеров на базе нашего салона в г.Курск</td>
                     <td></td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered">
                     <td style="padding: 5px; text-align:left;">Возможность обучения мастеров на базе нашего салона в г.Курск</td>
                     <td></td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered lined">
                     <td style="padding: 5px; text-align:left;">Маркетинг на 100%. Порядок формирования бюджета планирования, реализации и оценки эффективности маркетинговых мероприятий. Перечень основных видов маркетинговых мероприятий (ежегодно)</td>
                     <td></td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered">
                     <td style="padding: 5px; text-align:left;">Альбом причесок и стрижек</td>
                     <td></td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered lined">
                     <td style="padding: 5px; text-align:left;">Предоставление эксклюзива на город с численностью менее 1 млн.чел.</td>
                     <td></td>
                     <td></td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered">
                     <td style="padding: 5px; text-align:left;">Выезд специалиста в Ваш город для открытия салона «под ключ»</td>
                     <td></td>
                     <td></td>
                     <td>+</td>
                  </tr>
                  <tr class="bordered lined">
                     <td style="padding: 5px; text-align:left;">Обучение и внедрение бизнес процессов салона.</td>
                     <td></td>
                     <td>+</td>
                     <td>+</td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>



<!-- 2columns -->
<!--table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="2columns">
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table bgcolor="#ffffff" width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <tr>
                                 <td>

                                    <table width="290" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                       <tbody>
                                          <tr>
                                             <td width="100%" height="20"></td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <table width="290" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                   <tbody>
                                                      <tr>
                                                         <td width="290" height="160" align="center" class="devicewidth">
                                                            <img src="http://{url}/images/email/001/ipad.png" alt="" border="0" width="290" height="160" style="display:block; border:none; outline:none; text-decoration:none;" class="colimg2">
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td>
                                                            <table width="270" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                               <tbody>
                                                                  <tr>
                                                                     <td width="100%" height="20"></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #333333; line-height:24px;text-align:center;" st-title="2coltitle1">
                                                                        2 Columns Heading
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td width="100%" height="20"></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; line-height:24px; color: #666666; text-align:center;" st-conteent="2colcontent1">
                                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eius modtempor incididunt ut labore et dolore.
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td width="100%" height="20"></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #0a8cce; text-align:center;" st-title="2colreadmore1">
                                                                        <a href="#" style="text-decoration:none; color:#0a8cce;">Read More</a>
                                                                     </td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                    <table width="290" align="right" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                       <tbody>
                                          <tr>
                                             <td width="100%" height="20"></td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <table width="290" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidth">
                                                   <tbody>
                                                      <tr>
                                                         <td width="290" height="160" align="center" class="devicewidth">
                                                            <img src="http://{url}/images/email/001/ipad.png" alt="" border="0" width="290" height="160" style="display:block; border:none; outline:none; text-decoration:none;" class="colimg2">
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td>
                                                            <table width="270" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
                                                               <tbody>
                                                                  <tr>
                                                                     <td width="100%" height="20"></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td style="font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #333333;line-height:24px; text-align:center;" st-title="2coltitle2">
                                                                        2 Columns Heading
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td width="100%" height="20"></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; line-height:24px; color: #666666; text-align:center;" st-content="2colcontent2">
                                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eius modtempor incididunt ut labore et dolore.
                                                                     </td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td width="100%" height="20"></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td style="font-family: Helvetica, arial, sans-serif; font-size: 14px; color: #0a8cce; text-align:center;" st-title="2colreadmore2">
                                                                        <a href="#" style="text-decoration:none; color:#0a8cce;">Read More</a>
                                                                     </td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <tr>
                                 <td width="100%" height="10"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table-->
<!-- end of 2 columns -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
   <tbody>
      <tr>
         <td>
            <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
               <tbody>
                  <tr>
                     <td align="center" height="30" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td width="550" align="center" height="1" bgcolor="#d1d1d1" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td align="center" height="30" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of seperator -->
<!-- Start Full Text -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="full-text">
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <!-- Spacing -->
                              <tr>
                                 <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td>
                                    <table width="560" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
                                       <tbody>
                                          <!-- Title -->
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 30px; color: #333333; text-align:center; line-height: 30px;" st-title="fulltext-title">
                                                Требования к покупателям франшизы.
                                             </td>
                                          </tr>
                                          <!-- End of Title -->
                                          <!-- spacing -->
                                          <tr>
                                             <td width="100%" height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                                          </tr>
                                          <!-- End of spacing -->
                                          <!-- content -->
                                          <tr>
                                             <td style="font-family: Helvetica, arial, sans-serif; font-size: 16px; color: #666666; text-align:left; line-height: 30px;" st-content="fulltext-content">
                                                <ul>
                                                   <li>Интерес к быстроразвивающемуся сегменту салонного бизнеса;</li>
                                                   <li>Наличие необходимого объёма денежных средств для запуска и обеспечения поддержки проекта на начальном этапе, ответственность за исполнение взаимных обязательств;</li>
                                                   <li>Готовность выполнять немногочисленные и вполне разумные требования компании к франчайзи, поддерживать деловую репутацию компании и имидж марки;</li>
                                                   <li>Мы ищем партнеров, которые нацелены на результат и готовы упорно работать;</li>
                                                </ul>
                                             </td>
                                          </tr>
                                          <!-- End of content -->
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td height="20" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
                              </tr>
                              <!-- Spacing -->
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- end of full text -->
<!-- Start of seperator -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="seperator">
   <tbody>
      <tr>
         <td>
            <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
               <tbody>
                  <tr>
                     <td align="center" height="30" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td width="550" align="center" height="1" bgcolor="#d1d1d1" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
                  <tr>
                     <td align="center" height="30" style="font-size:1px; line-height:1px;">&nbsp;</td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of seperator -->
<!-- Start of Postfooter -->
<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="postfooter" >
   <tbody>
      <tr>
         <td>
            <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
               <tbody>
                  <tr>
                     <td width="100%">
                        <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                           <tbody>
                              <tr>
                                 <td align="center" valign="middle" style="font-family: Helvetica, arial, sans-serif; font-size: 14px;color: #666666" st-content="postfooter">
                                    Вы можете <a href="#" style="text-decoration: none; color: #0a8cce">отписаться</a> от рассылки.
                                 </td>
                              </tr>
                              <!-- Spacing -->
                              <tr>
                                 <td width="100%" height="20"></td>
                              </tr>
                              <!-- Spacing -->
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
            </table>
         </td>
      </tr>
   </tbody>
</table>
<!-- End of postfooter -->

</body>
</html>
