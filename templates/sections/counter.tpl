<div class="parallaxBg" >
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-12 titleSection  wow fadeInUp animated" data-wow-offset="100" data-wow-duration="1s" >
                    <h2 class="downloadNumber" >949,465 <br> DOWNLOADS</h2>
                    <div class="colorTitles ">
                        <span class="yellow">
                            Best App showcase theme
                        </span>
                    </div>
                    <p>since day one — Nov 1, 2014</p>
                </div>
            </div><!-- row end -->
        </div><!-- container end -->
    </div><!-- overlay end -->
</div>