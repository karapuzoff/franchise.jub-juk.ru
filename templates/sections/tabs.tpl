<section id="franchise">
   <div class="container">
      <div class="row">
         <div class="col-md-12 titleSection">
            <h2>Информация о нас</h2>
            <div class="coloredLine"></div>
         </div>
      </div>
   </div>
    
   <div class="space80"></div>
    
   <!--div class="container">
      <div class="row" >
         
         <div class="col-md-12">
            <ul class="servicesList">
               
               <a href="#salon" id="salon-tab" role="tab" data-toggle="tab" aria-controls="salon" aria-expanded="true">
                  <li class="col-md-4 col-sm-6 purple">
                     <span class="number">1</span>
                     <h3>Наши салоны</h3>
                  </li>
               </a>
               
               <a href="#franch" id="franch-tab" role="tab" data-toggle="tab" aria-controls="franch">
                  <li class="col-md-4 col-sm-6 pink">
                     <span class="number">2</span>
                     <h3>Франчайзинг</h3>
                  </li>
               </a>
               
               <a href="#garanty" id="garanty-tab" role="tab" data-toggle="tab" aria-controls="garanty">
                  <li class="col-md-4 col-sm-6 blue">
                     <span class="number">3</span>
                     <h3>Как начать работу</h3>
                  </li>
               </a>
               
            </ul>
         </div>
         
      </div>
   </div>
   
   <div class="space80"></div-->
   
   <div id="info-tabs" class="tab-content">
            
      <div role="tabpanel" class="tab-pane fade in active" id="salon" aria-labelledby="salon-tab">
         {salon}
      </div>
      
      <div role="tabpanel" class="tab-pane fade in active" id="franch" aria-labelledby="franch-tab">
      <div class="space80"></div>
         {franch}
      </div>
            
      <div role="tabpanel" class="tab-pane fade in active" id="garanty" aria-labelledby="garanty-tab">
      <div class="space80"></div>
         {work}
      </div>
         
   </div>

</section>