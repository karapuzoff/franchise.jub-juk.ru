<section id="team">
    <div class="container">
        <div class="row">
            <!-- team title -->
            <div class="col-md-12 titleSection">
                <h2> Team behind mix </h2>
                <p>Lorem Ipsum is simply dummy text of the printing and <span class="bold"> typesetting </span> industry!</p>
                <div class="coloredLine"></div>
            </div><!-- col-md-12 end -->
        </div><!-- row end -->
    </div><!-- container end -->
    <div class="space80"></div>
    <div class="container">
        <div class="row" >
            <!-- team carousel -->
            <div id="owl-carousel-team" class="owl-carousel owl-theme">
                <div class="item" >
                    <div class="team-item col-md-12" >
                        <div class="teamWrapper">
                            <img src="images/team/1.jpg"  class="img-responsive" alt=""/>
                            <div class="team-overlay"></div>
                            <div class="team-inner">
                                <h5><strong>Pearson</strong> Bell<span> Creative Director</span></h5>
                                <div class="thumb-overlay">
                                    <ul>
                                        <li><a href="index.html#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="index.html#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="index.html#"><i class="fa fa-dribbble"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- teamWrapper end -->
                    </div>
                </div><!-- item end -->
                <div class="item">
                    <div class="team-item col-md-12" >
                        <div class="teamWrapper">
                            <img src="images/team/2.jpg"  class="img-responsive" alt=""/>
                            <div class="team-overlay"></div>
                            <div class="team-inner">
                                <h5><strong>Randy</strong> Pearson<span>Founder</span></h5>
                                <div class="thumb-overlay">
                                    <ul>
                                        <li><a href="index.html#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="index.html#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="index.html#"><i class="fa fa-dribbble"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- teamWrapper end -->
                    </div>
                </div><!-- item end -->
                <div class="item">
                    <div class="team-item col-md-12" >
                        <div class="teamWrapper">
                            <img src="images/team/3.jpg" class="img-responsive" alt=""/>
                            <div class="team-overlay"></div>
                            <div class="team-inner">
                                <h5><strong>Jerry</strong> Torres<span> Developer</span></h5>
                                <div class="thumb-overlay">
                                    <ul>
                                        <li><a href="index.html#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="index.html#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="index.html#"><i class="fa fa-dribbble"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- teamWrapper end -->
                    </div>
                </div><!-- item end -->
                <div class="item" >
                    <div class="team-item col-md-12" >
                        <div class="teamWrapper">
                            <img src="images/team/4.jpg"  class="img-responsive" alt=""/>
                            <div class="team-overlay"></div>
                            <div class="team-inner">
                                <h5><strong>john</strong> Bell<span> Creative Director</span></h5>
                                <div class="thumb-overlay">
                                    <ul>
                                        <li><a href="index.html#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="index.html#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="index.html#"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- teamWrapper end -->
                    </div>
                </div><!-- item end -->
                <div class="item" >
                    <div class="team-item col-md-12" >
                        <div class="teamWrapper">
                            <img src="images/team/5.jpg" class="img-responsive" alt=""/>
                            <div class="team-overlay"></div>
                            <div class="team-inner">
                                <h5><strong>Randy</strong> Pearson<span>Founder</span></h5>
                                <div class="thumb-overlay">
                                    <ul>
                                        <li><a href="index.html#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="index.html#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="index.html#"><i class="fa fa-dribbble"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- teamWrapper end -->
                    </div>
                </div><!-- item end -->
            </div><!-- #owl-carousel-team end -->
        </div><!-- row end -->
    </div><!-- container end -->
</section>