<section id="thirdFeature">
   <div class="container thirdFeatureBg">
      <div class="row">

         <div class="col-md-6 col-md-offset-6">
            <div class="thirdFeatureWrapper">
               <div class="thirdFeatureText-header">
                  <h2 class="light">С кем мы <span class="bold">работаем</span>?</h2>
               </div>
               <div class="thirdFeatureText">
                  <ul class="feature-list">
                     <li class="wow fadeInRight animated" data-wow-offset="50" data-wow-duration="0.5s" >
                        <div class="row">
                           <div class="col-md-2"><h4><i class="fa fa-smile-o pink"></i></h4></div>
                           <div class="col-md-10">
                              <h4>
                                 Интерес к быстроразвивающемуся сегменту салонного бизнеса
                              </h4>
                           </div>
                        </div>
                     </li>
                     <li class="wow fadeInRight animated" data-wow-offset="50" data-wow-duration="1s" >
                        <div class="row">
                           <div class="col-md-2"><h4><i class="fa fa-rub green"></i></h4></div>
                           <div class="col-md-10">
                              <h4>
                                 Наличие необходимого объема денежных средств для запуска и обеспечения поддержки проекта на начальном
                                 этапе, ответственность за исполнение взаимных обязательств.
                              </h4>
                           </div>
                        </div>
                     </li>
                     <li class="wow fadeInRight animated" data-wow-offset="50" data-wow-duration="1.5s" >
                        <div class="row">
                           <div class="col-md-2"><h4><i class="fa fa-thumbs-up yellow"></i></h4></div>
                           <div class="col-md-10">
                              <h4>
                                 Готовность выполнять немногочисленные и вполне разумные требования компании к франчайзи, поддерживать
                                 деловую репутацию компании и имидж марки.
                              </h4>
                           </div>
                        </div>
                     </li>
                     <li class="wow fadeInRight animated" data-wow-offset="50" data-wow-duration="1.5s" >
                        <div class="row">
                           <div class="col-md-2"><h4><i class="fa fa-bullseye purple"></i></h4></div>
                           <div class="col-md-10">
                              <h4>
                                 Мы ищем партнеров, которые нацелены на результат и готовы упорно работать.
                              </h4>
                           </div>
                        </div>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>