<section id="secondFeature">
   <div class="container">
      <div class="row">

         <div class="secondFeatureRight">
            <img class="secondFeaturePhone" alt="img" src="/images/thinking-woman-3.png" />
         </div>

         <div class="secondFeatureLeft" >
            <h3 class="light">
               Ищите прибыльный бизнес в перспективной отрасли?<br />
               Хотите работать в сфере красоты, радости и позитива?<br />
               Стремитесь к финансовой независимости?<br />
               Ищите проверенную бизнес модель?
            </h3>
         </div>
         
      </div>
   </div>
</section>

<div class="clearfix secondFeatureClear"></div>