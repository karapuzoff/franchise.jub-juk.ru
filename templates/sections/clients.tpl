<section id="clients">
    <!-- Consider using h2-h6 elements to add identifying headings to all sections -->
    <h2 class="hiddenHeader">Clients</h2>
    <div class="container">
        <!-- clients logos -->
        <div class="row clientsLogos">
            <span>
                <a href="index.html#"><img alt="" src="images/clients/1.png" class="img-responsive imgClient"></a>
            </span>
            <span>
                <a href="index.html#"><img alt="" src="images/clients/2.png" class="img-responsive imgClient"></a>
            </span>
            <span>
                <a href="index.html#"><img alt="" src="images/clients/3.png" class="img-responsive imgClient"></a>
            </span>
            <span>
                <a href="index.html#"><img alt="" src="images/clients/4.png" class="img-responsive imgClient"></a>
            </span>
        </div><!-- row end -->
    </div><!-- container end -->
</section>