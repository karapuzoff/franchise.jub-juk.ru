<section id="videoSection" >
   <div class="container">
      <div class="row">

         <div class="col-md-6 wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s">
            <div class="video-container">
               <div class="video">
                  <iframe width="640" height="390" src="https://www.youtube.com/embed/w15qFmrKyMQ" frameborder="0" allowfullscreen></iframe>
               </div>
            </div>
         </div>

         <div class="col-md-6 wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s">
            <h3>Наша миссия</h3>
            <p>
               Научить маленького Клиента любить свою внешность, ухаживать за собой, меняться, преображаться и не боятся быть ярким
            </p>
            <h3>Наша цель</h3>
            <p>
               Дать возможность ребенку почувствовать себя настоящим героем, вокруг которого происходит основное действие.
            </p>
         </div>

      </div>
   </div>
</section>