<section id="contact" class="parallaxBg3">
   <div class="overlay">
        
      <div class="container">
         <div class="row">
            <div class="col-md-12 infoDetails" >
               <h1>НЕ ТЕРЯЙТЕ ВРЕМЕНИ!</h1>
               <h3>Свяжитесь с нами прямо сейчас и получите бесплатную консультацию наших специалистов!</h3>
               <div class="space40"></div>
               <div class="row">
                  <div class="col-md-6">
                     <form role="form" name="contactform" action="/ajax/send.ajax.php" id="contactForm-0">
                        <div class="form-group col-sm-12" id="name-group-0">
                           <input type="text" id="inputName" name="inputName" placeholder="Ваше имя" />
                        </div>
                        <div class="form-group col-sm-12" id="phone-group-0">
                           <input type="text" id="inputPhone" name="inputPhone" placeholder="Ваш телефон" />
                        </div>
                        <div class="form-group col-sm-12" id="email-group-0">
                           <input type="email" id="inputEmail" name="inputEmail" placeholder="Ваш Email" />
                        </div>
                        <div class="col-md-12 submit">
                           <button class="btn-new btn-bold" style="text-transform: uppercase" type="submit" id="submit-0" name="submit">Получить презентацию!</button>
                        </div>
                     </form>
                  </div>
                  <div class="col-md-6">
                     <!--div style="position: relative; margin: -25px 0; top: 20px;">
                        <img src="/images/woman-down.png" style="" />
                        <img src="/images/woman-calling.png" style="" />
                     </div-->
                     <span class="phone">
                        <i class="fa fa-phone"></i>
                        8 800 333 5612
                        <span>Звонок по РФ бесплатный</span>
                     </span>
                     <h3>
                        <i class="fa fa-envelope"></i>
                        yulya4886@mail.ru
                     </h3>
                     <h3>
                        <i class="fa fa-globe"></i>
                        jub-jik.ru
                     </h3>
                     <!--h2>Курск, ул. Радищева, 52</h2-->
                     <img src="/images/logo-mini.png" />
                  </div>
               </div>
               <!--h3>franchise@jub-jik.ru</h3>
               <!--h2>Курск, ул. Радищева, 52</h2>
               <img src="/images/logo-mid-2.png" /-->   
            </div>
            <div class="space80"></div>
            <div class="container">
                <div class="col-md-12">
                    <p>
                       2015 <i class="fa fa-copyright"></i> Детская парикмахерская «Чуб-Чик».</p>
                    <div class="space20"></div>
                    <ul  class="circleSocial">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="space40"></div>
         </div>
      </div>
      
      <!--div class="container-fluid contactBg" >
         <div class="container">
            <div class="row">
               
               <form id="contactForm" role="form" >
                  <div class="col-sm-12">   
                     <h4 class="success"><i class="fa fa-check"></i> Удачно</h4>
                     <h4 class="error"><i class="fa fa-warning"></i> Ошибка</h4>
                  </div>
                  <div class="col-sm-4">
                     <input  id="name" type="text" name="name" placeholder="Ваше имя">
                  </div>
                  <div class="col-sm-4">
                     <input  id="email" type="email" name="email" placeholder="Ваш телефон">
                  </div>
                  <div class="col-sm-4">
                     <input  id="email" type="email" name="email" placeholder="Ваш Email">
                  </div>
                  <div class="col-md-12">
                     <button class="btn-new btn-bold" style="text-transform: uppercase" type="submit" id="submit" name="submit">Получить презентацию!</button>
                  </div>
               </form>
            
            </div>
         </div>
      </div-->
      
   </div>
</section>