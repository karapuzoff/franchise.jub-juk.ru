<section id="firstFeature">
   <div class="container">
      <div class="row">

         <div class="col-md-4 col-xs-6 welcomeText">
            <div class="colorTitles">
               <img src="/images/bubble-01.png" class="wow bounceInLeft animated" data-wow-offset="120" data-wow-duration="1s" />
               <!--span class="txt txt-blue wow bounceInLeft animated" data-wow-offset="120" data-wow-duration="1s">
                  Ищите прибыльный бизнес?
               </span-->
               <br />
               <img src="/images/bubble-02.png" class="wow bounceInLeft animated" data-wow-offset="120" data-wow-duration="1.5s" />
               <!--span class="txt txt-blue wow bounceInLeft animated" data-wow-offset="120" data-wow-duration="1.5s"  >
                  Хотите работать в сфере красоты, радости и позитива?
               </span-->
            </div>
         </div>

         <div id="thinking-woman" class="col-md-3 col-xs-12">
            <div class="phones">
               <img class="phone-front wow fadeInUp animated" data-wow-offset="120" data-wow-duration="1.5s" alt="img" src="/images/thinking-woman-3.png" />
            </div>
         </div>

         <div class="col-md-5 col-xs-6 welcomeText">
            <div class="colorTitles">
               <img src="/images/bubble-03.png" class="wow bounceInRight animated" data-wow-offset="120" data-wow-duration="1s" />
               <!--span class="txt txt-blue wow bounceInRight animated" data-wow-offset="120" data-wow-duration="1s"   >
                  Стремитесь к финансовой независимости?
               </span-->
               <br />
               <img src="/images/bubble-04.png" class="wow bounceInRight animated" data-wow-offset="120" data-wow-duration="1.5s" />
               <!--span class="txt txt-pink wow bounceInRight animated" data-wow-offset="120" data-wow-duration="1.5s"  >
                  Ищите проверенную прибыльную бизнес-модель?
               </span-->
            </div>
            
            <!--a href="#contact" class=" page-scroll btn-new">
               Купить франшизу
            </a-->
         </div>
         
      </div>
   </div>
</section>