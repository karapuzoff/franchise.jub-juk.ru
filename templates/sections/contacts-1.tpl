<section id="contact-intro" class="parallaxBg4">

        
      <div class="container-fluid contactBg" >
         <div class="container">
            <div class="row">
               
               <form id="contactForm-{id}" role="form" >
                  <div class="form-group col-sm-4" id="name-group-{id}">
                     <input type="text" id="inputName" name="inputName" placeholder="Ваше имя" />
                  </div>
                  <div class="form-group col-sm-4" id="phone-group-{id}">
                     <input type="text" id="inputPhone" name="inputPhone" placeholder="Ваш телефон" />
                  </div>
                  <div class="form-group col-sm-4" id="email-group-{id}">
                     <input type="email" id="inputEmail" name="inputEmail" placeholder="Ваш Email" />
                  </div>
                  <div class="col-md-12">
                     <button class="btn-new btn-bold" style="text-transform: uppercase" type="submit" id="submit-{id}" name="submit">Узнайте подробнее!</button>
                  </div>
               </form>
            
            </div>
         </div>
      </div>

</section>