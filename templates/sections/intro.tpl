<section id="intro" class="parallaxBg">
   <div class="overlay">
      <div class="container">
         <div class="row">
            
            <div class="navbar-header pull-right text-left">
               <span class="navbar-brand">
                  <a href="tel:88003335612" style="font-size: 1.3em; font-weight: 400">
                     <i class="fa fa-phone-square fa-lg"></i>
                     8 (800) 333-5612
                  </a>
                  <br />
                  <div class="txt txt-blue bold" style="font-size: 1.3rem; padding-left: 4.5rem">
                     Звонок по РФ бесплатный
                  </div>
               </span>
            </div>
            <div id="jub-jik" class="collapse navbar-collapse jub-jik">
               <ul class="nav navbar-nav">
                  <li><a href="//jub-jik.ru">Сайт "Чуб-Чик"</a></li>
               </ul>
            </div>
                
            <div class="col-md-8 middle-intro">
               <h1 class="txt txt-blue">
                  Откройте<br />
                  <b class="txt-pink uppercase">собственную парикмахерскую</b><br />
                  <img class="logo" style="width: 15rem; margin: 0.5rem 0 1.2rem;" alt="logo" src='/images/logo-mid.png' title="Чуб-Чик" />
                  для детей по франшизе<br />
                  <b class="txt-red">с доходом от 100 000 рублей.</b>
               </h1>
                    
               <!--img class="logo" style="width: 50rem" alt="logo" src='/images/logo-mid.png' title="Чуб-Чик" /-->

               <div id="clock" class="clock-red">
                  <h3 class="txt-red" style="margin-bottom: 30px;">
                     Успей отправить заявку!<br />
                     И получи 3 месяца работы без роялти.<br />
                     До конца акции осталось
                  </h3>
                  <div class="clock" id="clock-intro"></div>
               </div>

               <a class="page-scroll btn-new btn-bold" style="margin-top: 3rem" href="#contact">
                  <strong style="text-transform: uppercase"><i class="fa fa-check fa-lg"></i> Отправить заявку</strong> и получить 3 месяца работы <strong>без роялти.</strong>
               </a>
                
            </div>
            
         </div>
      </div>

   </div>
</section>