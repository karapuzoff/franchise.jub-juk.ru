<section id="download" class="parallaxBg2">
   <div class="overlay">
      <div class="container">
         <div class="row">
            <div class="col-md-12 wow fadeInUp animated" data-wow-offset="100" data-wow-duration="1.5s">
               <h1>
                  Успейте купить!
               </h1>
               
               <div class="space20"></div>
               
               <a class="page-scroll btn-new btn-bold" style="margin-top: 3rem" href="#contact">
                  <i class="fa fa-check fa-lg"></i>
                  <strong style="text-transform: uppercase">Отправить заявку</strong> и получить 3 месяца работы <strong>без роялти</strong>.
               </a>
            </div>
         </div>
      </div>
   </div>
</section>