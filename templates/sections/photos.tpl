<section id="screenshots">
   <div class="container">
      <div class="row">
         <div class="col-md-12 titleSection">
            <h2>Наши салоны</h2>
            <div class="coloredLine"></div>
         </div>
      </div>
   </div>
    
   <div class="space80"></div>
    
   <div class="container">
      <div class="row" >
         <div id="owl-carousel-works" class="owl-carousel owl-theme">
            
            <div class="item">
               <div class="latest-item col-md-12" >
                  <a href="/images/salon/photo-01.jpg" title="Фото #1" data-lightbox-gallery="screenshots-gallery">
                     <img src="/images/salon/photo-01.jpg" class="img-responsive" alt=""/>
                  </a>
               </div>
            </div>
            
            <div class="item">
               <div class="latest-item col-md-12" >
                  <a href="/images/salon/photo-02.jpg" title="Фото #1" data-lightbox-gallery="screenshots-gallery">
                     <img src="/images/salon/photo-02.jpg" class="img-responsive" alt=""/>
                  </a>
               </div>
            </div>
            
            <div class="item">
               <div class="latest-item col-md-12" >
                  <a href="/images/salon/photo-03.jpg" title="Фото #1" data-lightbox-gallery="screenshots-gallery">
                     <img src="/images/salon/photo-03.jpg" class="img-responsive" alt=""/>
                  </a>
               </div>
            </div>
            
            <div class="item">
               <div class="latest-item col-md-12" >
                  <a href="/images/salon/photo-04.jpg" title="Фото #1" data-lightbox-gallery="screenshots-gallery">
                     <img src="/images/salon/photo-04.jpg" class="img-responsive" alt=""/>
                  </a>
               </div>
            </div>
            
            <div class="item">
               <div class="latest-item col-md-12" >
                  <a href="/images/salon/photo-05.jpg" title="Фото #1" data-lightbox-gallery="screenshots-gallery">
                     <img src="/images/salon/photo-05.jpg" class="img-responsive" alt=""/>
                  </a>
               </div>
            </div>

         </div>
      </div>
   </div>
</section>