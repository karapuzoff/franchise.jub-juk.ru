<section class="parallaxBg" >
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- testimonials carousel -->
                    <div id="owl-carousel-testimonials" class="owl-carousel" >
                        <div>
                            <img  src="images/testimonials/1.jpg"  class="img-responsive" alt=""/>
                            <h5>Liz Watson</h5>
                            <p>It is a long <span class="bold"> established fact</span> that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p>
                        </div>
                        <div>
                            <img  src="images/testimonials/2.jpg"  class="img-responsive" alt=""/>
                            <h5>John Doe</h5>
                            <p>There are <span class="bold">  many variations </span> of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by <span class="bold">  injected humour, or randomised words </span> which don't look even slightly believable</p>
                        </div>
                        <div>
                            <img  src="images/testimonials/3.jpg"  class="img-responsive" alt=""/>
                            <h5>Mary vizy</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard <span class="bold">  dummy text </span> ever since the 1500s, when an unknown printer took a galley of type</p>
                        </div>
                    </div><!-- owl-carousel-testimonials end -->
                </div><!-- col-md-12 end -->
            </div><!-- row end -->
        </div><!-- container end -->
    </div><!-- overlay end -->
</section>