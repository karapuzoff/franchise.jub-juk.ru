<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-md-12 titleSection">
                <h2>What did you recieve with it</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry!</p>
                <div class="coloredLine"></div>
            </div>
        </div>
    </div>
    <div class="space80"></div>

    <div class="servicesContent" >
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!--the 4 servicesList -->
                    <ul class="servicesList">
                        <li class="col-md-3 col-sm-6 purple">
                            <span class="number">1</span>
                            <h3> Responsive</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                        </li>
                        <li class="col-md-3 col-sm-6 pink">
                            <span class="number">2</span>
                            <h3> Clean</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                        </li>
                        <li class="col-md-3 col-sm-6 blue">
                            <span class="number">3</span>
                            <h3> Simple</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                        </li>
                        <li class="col-md-3 col-sm-6 yellow">
                            <span class="number">4</span>
                            <h3> Unique</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</section>