<section id="plans">
   <div class="container">
      <div class="row">
         <div class="col-md-12 titleSection">
            <h2>Финансовые условия</h2>
            <p>Мы предлагаем 3 варианта сотрудничества.</p>
            <div class="coloredLine"></div>
         </div>
      </div>
   </div>
    
   <div class="space80"></div>
    
   <div class="container">
      <div class="row">
            
         <div class="col-sm-4">
            <div class="item">
               <div class="price">
                  <p>Лайт</p>
               </div>
               <h3>Помещение – 35-40 кв.м.</h3>
               <ul class="plans-list">
                  <li>
                     2 детских кресла + 1 взрослое (только парикмахерский зал)
                  </li>
                  <li><h3>
                     Инвестиции:<br />
                     <span class="lead">245 000 - 295 000 руб.</span>
                  </h3></li>
                  <li><h3>
                     Паушальный взнос:<br />
                     <span class="lead">90 000 руб.</span>
                  </h3></li>
                  <li><h3>
                     Роялти:<br />
                     <span class="lead">6 000 руб.</span>
                  </h3></li>
               </ul>
               <a class="page-scroll btn-new" href="#contact">КУПИТЬ</a>
            </div>
         </div>
         
         <div class="col-sm-4">
            <div class="item popular">
               <div class="price">
                  <p>Стандарт</p>
               </div>
               <h3>Помещение – 50-60 кв.м.</h3>
               <ul class="plans-list">
                  <li>
                     3 детских кресла + 2 взрослых + маникюрный кабинет.
                  </li>
                  <li><h3>
                     Инвестиции:<br />
                     <span class="lead">550 000 - 580 000 руб.</span>
                  </h3></li>
                  <li><h3>
                     Паушальный взнос:<br />
                     <span class="lead">140 000 руб.</span>
                  </h3></li>
                  <li><h3>
                     Роялти:<br />
                     <span class="lead">10 000 руб.</span>
                  </h3></li>
               </ul>
               <a class="page-scroll btn-new" href="#contact">КУПИТЬ</a>
            </div>
         </div>
         
         <div class="col-sm-4">
            <div class="item">
               <div class="price">
                  <p>Премиум</p>
               </div>
               <h3>Помещение – 70-100 кв.м.</h3>
               <ul class="plans-list">
                  <li>
                     4 детских кресла + 3 взрослых + маникюрный кабинет + тату-кабинет.
                  </li>
                  <li><h3>
                     Инвестиции:<br />
                     <span class="lead">750 000 - 940 000 руб.</span>
                  </h3></li>
                  <li><h3>
                     Паушальный взнос:<br />
                     <span class="lead">350 000 руб.</span>
                  </h3></li>
                  <li><h3>
                     Роялти:<br />
                     <span class="lead">25 000 руб.</span>
                  </h3></li>
               </ul>
               <a class="page-scroll btn-new" href="#contact">КУПИТЬ</a>
            </div>
         </div>

      </div>
   </div>
</section>