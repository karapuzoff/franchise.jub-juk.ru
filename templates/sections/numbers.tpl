<section id="numbers" class="parallaxBg4" >
    <div class="overlay">
        <!-- title -->
        <div class="container-fluid numbers-title" >
            <div class="container">
                <div class="row">
                    <h1>
                       Несколько счетчиков, например
                    </h1>
                </div>
            </div>
        </div>
        <div class="space80"></div>
        <div class="container-fluid">
            <!-- number list -->
            <ul class="numbersList">
                <li class="col-md-4 col-xs-6">
                    <h4>Продано франшиз</h4>
                    <span id="number1">0</span>
                </li>
                <li class="col-md-4 col-xs-6">
                    <h4>Открыто салонов</h4>
                    <span id="number2">0</span>
                </li>
                <li class="col-md-4 col-xs-6">
                    <h4>Средняя прибыль салона</h4>
                    <span id="number3">0</span> тыс. руб. в месяц
                </li>
            </ul><!-- numbersList end -->
        </div><!-- container-fluid end -->
    </div><!-- overlay end -->
</section>