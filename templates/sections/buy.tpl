<section id="buy" class="parallaxBg2">
   <div class="overlay">
      <div class="container">
         <div class="row">
            <div class="col-md-12 wow fadeInUp animated" data-wow-offset="100" data-wow-duration="1.5s">
               <h1>
                  При подачи заявки до 1 сентября первые 3 месяца работы –<br />без роялти!
               </h1>
               
               <div class="space20"></div>
               
               <a class="page-scroll btn-new btn-bold" style="margin-top: 3rem" href="#contact">
                  <i class="fa fa-check fa-lg"></i>
                  <strong style="text-transform: uppercase">Отправить заявку</strong> и получить 3 месяца работы <strong>без роялти</strong>.
               </a>
               
               <!--a class="page-scroll btn btn-app-download" href="#contact">
                  <i class="fa fa-shopping-cart"></i>
                  <strong>КУПИТЬ СЕЙЧАС</strong><span>Акция до 1 сентября</span>
               </a-->
            </div>
         </div>
      </div>
   </div>
   <div id="clock" class="clock-black">
      <div class="clock" id="clock-buy"></div>
      <h3>до конца акции</h3>
   </div>
</section>