<section id="features" >
    <div class="container" >
        <div class="row">
            <!-- left features -->
            <div class="col-md-4 features-left">
                <ul>
                    <li class="wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="0.5s" >
                        <div class="features-ico"><i class="fa fa-magic yellow"></i></div>
                        <div class="features-info">
                            <h4>Design</h4>
                            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s </p>
                        </div>
                    </li>
                    <li class="wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1s" >
                        <div class="features-ico"><i class="fa fa-desktop purple"></i></div>
                        <div class="features-info">
                            <h4>Responsive Design</h4>
                            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </li>
                    <li class="wow fadeInLeft animated" data-wow-offset="10" data-wow-duration="1.5s" >
                        <div class="features-ico"><i class="fa fa-book blue"></i></div>
                        <div class="features-info">
                            <h4>Fully Documented</h4>
                            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </li>

                </ul>
            </div><!-- col-md-4 features-left end -->
            <!--  features mobile  -->
            <div id="featuresMobile" class="col-md-4 wow fadeInUp animated" data-wow-offset="100" data-wow-duration="1s" >
                <img alt="application" class="img-responsive mobile" src="images/phones/windows-red.png">
            </div><!-- col-md-4 features-left end -->
            <!-- right features -->
            <div class="col-md-4 features-right">
                <ul>
                    <li class="wow fadeInRight animated" data-wow-offset="10" data-wow-duration="0.5s" >
                        <div class="features-ico"><i class="fa fa-star yellow"></i></div>
                        <div class="features-info">
                            <h4>Excellent Features</h4>
                            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s </p>
                        </div>
                    </li>
                    <li class="wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1s" >
                        <div class="features-ico"><i class="fa fa-globe purple"></i></div>
                        <div class="features-info">
                            <h4>SEO</h4>
                            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s </p>
                        </div>
                    </li>
                    <li class="wow fadeInRight animated" data-wow-offset="10" data-wow-duration="1.5s" >
                        <div class="features-ico"><i class="fa fa-code blue"></i></div>
                        <div class="features-info">
                            <h4>Clean Code</h4>
                            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </li>
                </ul>
            </div><!-- col-md-4 features-right end -->
        </div><!-- row end -->
    </div><!-- container end -->
</section>